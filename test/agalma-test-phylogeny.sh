#!/bin/sh

set -e

DIR=`mktemp -d $PWD/agalma-test-phylogeny-XXX`

echo "Created temp directory '$DIR'"

if [ -n "$BIOLITE_RESOURCES" ]
then
	SAVE_BIOLITE_RESOURCES=",$BIOLITE_RESOURCES"
fi
export BIOLITE_RESOURCES="database=$DIR/biolite.sqlite,agalma_database=$DIR/agalma.sqlite,outdir=$DIR$SAVE_BIOLITE_RESOURCES"

agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-ATCACG" --species "Abylopsis tetragona" --ncbi_id "316209" --itis_id "51370" --extraction_id "FEG339" --library_id "FEG339" --library_type "transcriptome" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core" --sample_prep "Invitrogen Dynabeads mRNA DIRECT kit ; 1 round | Illumina TruSeq RNA Sample Prep Kit" -p ./HWI-ST625-54-C026EACXX-6-ATCACG.fa
agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-CAGATC" --species "Cordagalma sp." --itis_id "51393" --extraction_id "FEG328" --library_id "FEG328" --library_type "transcriptome" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core" --sample_prep "Invitrogen Dynabeads mRNA DIRECT kit ; 1 round | Illumina TruSeq RNA Sample Prep Kit" -p ./HWI-ST625-54-C026EACXX-6-CAGATC.fa
agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-CTTGTA" --species "Diphyes dispar" --ncbi_id "316177" --itis_id "51307" --extraction_id "Meghan Powers" --library_id "Meghan Powers" --library_type "transcriptome" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core" --sample_prep "NEB Magnetic mRNA Isolation Kit ; 1 round | Illumina TruSeq RNA Sample Prep Kit" -p ./HWI-ST625-54-C026EACXX-6-CTTGTA.fa
agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-GCCAAT" --species "Chuniphyes multidentata" --ncbi_id "316200" --itis_id "51304" --extraction_id "FEG325" --library_id "FEG325" --library_type "transcriptome" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core" --sample_prep "Invitrogen Dynabeads mRNA DIRECT kit ; 1 round | Illumina TruSeq RNA Sample Prep Kit" -p ./HWI-ST625-54-C026EACXX-6-GCCAAT.fa
agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-TTAGGC" --species "Apolemia sp." --itis_id "51421" --extraction_id "FEG355" --library_id "FEG355" --library_type "transcriptome" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core" --sample_prep "Invitrogen Dynabeads mRNA DIRECT kit ; 1 round & Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 2 rounds | Illumina TruSeq RNA Sample Prep Kit" -p ./HWI-ST625-54-C026EACXX-6-TTAGGC.fa
catalog insert --id "JGI_NEMVEC" --species "Nematostella vectensis" --ncbi_id "45351" --itis_id "52498" --library_type "genome" --note "Filtered transcript predictions from JGI used in Nematostella paper (Rebecca et al).  See paper for details" -p ./JGI_NEMVEC.fa

cd $DIR

agalma load --id HWI-ST625-54-C026EACXX-6-ATCACG
agalma load --id HWI-ST625-54-C026EACXX-6-CAGATC
agalma load --id HWI-ST625-54-C026EACXX-6-CTTGTA
agalma load --id HWI-ST625-54-C026EACXX-6-GCCAAT
agalma load --id HWI-ST625-54-C026EACXX-6-TTAGGC
agalma postassemble --id JGI_NEMVEC --skip exemplars
agalma load --id JGI_NEMVEC --previous JGI_NEMVEC --generic

LOAD_IDS=`diagnostics list -n load 2>/dev/null | awk 'NR>1 {print $2}'`
ID=PhylogenyTest

agalma homologize --id $ID --load_ids $LOAD_IDS
agalma multalign --id $ID
agalma genetree --id $ID
agalma treeprune --id $ID
agalma multalign --id $ID --supermatrix
agalma genetree --id $ID --raxml_flags="-o Nematostella_vectensis"

agalma report --id PhylogenyTest --outdir report
agalma resource_report --id PhylogenyTest --outdir report
agalma phylogeny_report --id PhylogenyTest --outdir report

