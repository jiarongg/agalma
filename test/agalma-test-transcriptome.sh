#!/bin/sh

set -e

DIR=`mktemp -d $PWD/agalma-test-transcriptome-XXX`

echo "Created temp directory '$DIR'"

if [ -n "$BIOLITE_RESOURCES" ]
then
	SAVE_BIOLITE_RESOURCES=",$BIOLITE_RESOURCES"
fi
export BIOLITE_RESOURCES="database=$DIR/biolite.sqlite,agalma_database=$DIR/agalma.sqlite,outdir=$DIR$SAVE_BIOLITE_RESOURCES"

agalma catalog insert --path ./HippopodiusTestSmall1.fastq ./HippopodiusTestSmall2.fastq --species "Hippopodius hippopus" --ncbi_id "168745" --itis_id "51331" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core"

cd $DIR

agalma transcriptome \
	--id HWI-ST625-54-C026EACXX-8-ATCACG \
	--subset_size 10000 \
	--stats_subset_size 5000 \
	--assemble_subsets 5000

agalma report --id HWI-ST625-54-C026EACXX-8-ATCACG --outdir report

