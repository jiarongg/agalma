set -e

DIR=/tmp/agalma.test.assemble

if [ -d $DIR ]; then
	echo "Directory $DIR already exists!"
	echo "Please remove and rerun."
	exit 0
fi

mkdir -p $DIR

export BIOLITE_RESOURCES="database=$DIR/biolite.sqlite,agalma_database=$DIR/agalma.sqlite,outdir=$DIR"

agalma catalog insert --path ./HippopodiusTestSmall1.fastq ./HippopodiusTestSmall2.fastq --species "Hippopodius hippopus" --ncbi_id "168745" --itis_id "51331" --sequencer "Illumina HiSeq 2000" --seq_center "Brown Genomics Core"

cd $DIR

ID=HWI-ST625-54-C026EACXX-8-ATCACG

agalma sanitize --id $ID
agalma insert_size --id $ID --previous $ID
agalma remove_rrna --id $ID --previous $ID
agalma assemble --id $ID --previous $ID --subsets 5000
agalma postassemble --id $ID --previous $ID

