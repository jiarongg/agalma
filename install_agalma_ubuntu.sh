#!/bin/sh

AGALMA="agalma-0.3.2"
FILE="$AGALMA.tar.gz"
URL="https://bitbucket.org/caseywdunn/agalma/downloads/$FILE"

set -e

if [ "$USER" != "root" ]; then
	echo "You must run this install script as root (e.g. with 'sudo')."
	exit 1
fi

LOG=`mktemp /tmp/install-agalma-XXXX`
echo "+ Install log is located in: $LOG"

if [ -f ./autogen.sh ]
then
	echo "+ Found local repository for Agalma: running autogen..."
	./autogen.sh >>$LOG
else
	echo "+ Retrieving remote tar ball for Agalma..."
	echo "+ Downloading $URL to /usr/local/src"
	mkdir -p /usr/local/src
	cd /usr/local/src
	wget $URL >>$LOG

	echo "+ Extracting $FILE"
	tar xf $FILE
	cd $AGALMA
fi

echo "+ Installing Agalma in /usr/local"
./configure >>$LOG
make install >>$LOG

echo "+ Finished installation.

  The 'agalma' wrapper should now be in your PATH (in /usr/local/bin).
"

