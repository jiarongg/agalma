#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

'''
Builds gene trees for each set of homologous sequences, it builds a
phylogenetic tree using the maximum likelihood optimality criterion as
implemented in RAxML; see http://www.exelixis-lab.org/ and
doi:10.1093/bioinformatics/btl446
'''

import glob
import os

from Bio import AlignIO

from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import wrappers

from biolite.pipeline import Pipeline

pipe = Pipeline('genetree', __doc__)

# Command-line arguments
pipe.add_argument('align_id', default=None, metavar='ID', help="""
	Use the alignments from a previous multalign run.""")
pipe.add_argument('align_dir', default=None, metavar='DIR', help="""
	Use an explicit path to a directory containing a FASTA (.fa) file for
	each alignment.""")
pipe.add_argument('seq_type', default='protein', help="""
	Choose whether gene trees will be built using protein or nucleotide
	sequences.""")
pipe.add_argument('model', short='m', default='PROTGAMMAWAG', help="""
	Choose a substitution model of evolution for RAxML.""")
pipe.add_argument('raxml_flags', default=None, help="""
	Extra parameters to pass to RAxML. Make sure the flags make sense: refer to
	the RAxML Manual for help.""")


# Pipeline stages
@pipe.stage
def setup_path(id, align_dir, align_id, seq_type):
	"""Determine the path of the input alignments"""

	if not align_id:
		prev = diagnostics.lookup_last_run(id, 'multalign')
		if prev:
			align_id = int(prev.run_id)
			utils.info("using previous 'multalign' run id %d" % align_id)

	if align_dir:
		align_dir = os.path.abspath(align_dir)
	elif align_id:
		# Lookup values for a previous run
		values = diagnostics.lookup(align_id, diagnostics.EXIT)
		if seq_type == 'protein':
			try:
				align_dir = values['protdir']
			except KeyError:
				utils.die("No 'protdir' entry found in run %s" % align_id)
		elif seq_type == 'nucleotide':
			try:
				align_dir = values['nucdir']
			except KeyError:
				utils.die("No 'nucdir' entry found in run %s" % align_id)
	else:
		utils.die(
			"No directory or run id provided: use --align_dir or --align_id")
		
	ingest('align_dir')


@pipe.stage
def convert_format(align_dir, outdir):
	"""Convert alignments from fasta to phylip format"""
	
	phylip_dir = "phylip-aligns"
	utils.safe_mkdir(phylip_dir)

	for fasta in glob.glob(os.path.join(align_dir, '*.fa')):
		basename = os.path.splitext(os.path.basename(fasta))[0]
		phylip = os.path.join(phylip_dir, basename + '.phy')
		# Convert to phylip-relaxed to allow taxon names longer than 10	
		try:
			AlignIO.convert(fasta, 'fasta', phylip, 'phylip-relaxed')
		except ValueError:
			utils.info("warning: skipping empty cluster '%s'" % basename)
	
	ingest('phylip_dir')


@pipe.stage
def genetrees(phylip_dir, seq_type, model, raxml_flags, outdir):
	"""Build gene trees with RAxML"""
	
	prot_models = ('PROTCATWAG', 'PROTCATLG',
	               'PROTGAMMAWAG', 'PROTGAMMALG')
	nuc_models = ('GTRMIX', 'GTRCAT', 'GTRGAMMA')

	if seq_type == 'protein' and model not in prot_models:
		utils.die(
			"%s is not a valid model for %s sequences" % (model, seq_type))
	elif seq_type == 'nucleotide' and model not in nuc_models:
		utils.die(
			"%s is not a valid model for %s sequences" % (model, seq_type))

	raxml_dir = os.path.join(outdir, 'raxml-trees')
	utils.safe_mkdir(raxml_dir)

	for phylip in glob.glob(os.path.join(phylip_dir, '*.phy')):
		basename = os.path.splitext(os.path.basename(phylip))[0]
		wrappers.Raxml(phylip, basename, model, raxml_dir, extra_flags=raxml_flags)

	finish('raxml_dir') 


if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('outdir', diagnostics.INIT, 'outdir')
		self.lookup('raxml_dir', diagnostics.EXIT, 'raxml_dir')
		# Generators
		self.generator(self.supermatrix_tree)

	def supermatrix_tree(self):
		"""
		Maximum-likelihood tree for the supermatrix.
		"""
		if self.check('raxml_dir'):
			newick = os.path.join(
						self.data.raxml_dir,
						'RAxML_bestTree.supermatrix')
			if os.path.exists(newick):
				self.add_js('jsphylosvg-min.js')
				self.add_js('raphael-min.js')
				return ["""
					<div id="run_{0}_canvas"></div>
					<script type="text/javascript">
					var run_{0}_newick="{1}";
					Smits.PhyloCanvas.Render.Style.text["font-style"]="italic";
					run_{0}_canvas = new Smits.PhyloCanvas(
						run_{0}_newick,
						"run_{0}_canvas",
						800, 800);
					</script>
					""".format(
						self.run_id,
						open(newick).readline().rstrip().replace('_',' '))]

