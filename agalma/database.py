# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Provides an interface to the underlying SQLite database that stores the
Agalma **sequences**, **rpsblast**, and **homology** tables.
"""

import os
import re
import sqlite3
 
from collections import namedtuple

from biolite import utils
from biolite.config import get_resource

_db = None

def _create_sql(name, schema, index):
	sql = [" CREATE TABLE %s (" % name]
	sql += ["   %s %s," % s for s in schema[:-1]]
	sql += ["   %s %s);" % schema[-1]]
	sql += [" CREATE INDEX {0}_{1} ON {0}({1});".format(name, i) for i in index]
	return '\n'.join(sql)

sequences_schema = (
	('sequence_id', 'INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL'),  
	('run_id', 'INTEGER'),			# Foreign key to runs table, refers to the run_id of the process that populates the database
	('catalog_id', 'VARCHAR(256)'),			# Foreign key to catalog.id, could be obtained through join with run table but including it here makes things more robust
	('locus', 'INTEGER'),			# Parsed from the header, eg 3 from Locus_3_Transcript_20
	('transcript', 'INTEGER'),			# Parsed from the header, eg 20 from Locus_3_Transcript_20
	('confidence', 'FLOAT'),			# Parsed from the header
	('rpkm', 'FLOAT'),			# Parsed from the header, null for non-exemplars
	('exemplar', 'INTEGER'),			# 0 if not an exemplar, 1 if an exemplar (all rows will be 1 if loading from the exemplar file)
	('nucleotide_seq', 'TEXT'),			# Nucleotide sequence from either exemplars or transcripts file (which is loaded will be up to the user)
	('nucleotide_quality', 'TEXT'),			# Quality string corresponding to nucleotide_seq, NULL for most assemblers (including oases)
	('protein_seq', 'TEXT'),			# Parsed from the prot4EST output, for now will be null for non-exemplars since they aren't translated. 
	('translation_method', 'VARCHAR(256)'),			# A string with the program and method, eg "prot4EST SIMILARITY", only applies when protein_seq is not NULL
	('organelle', 'VARCHAR(256)'),			# Either nuclear or mitochondrial, parsed from prot4EST output. NULL for non exemplars.
	('blast_hit', 'TEXT'),			# The name, gi, and evalue of the top blastx hit, could be parsed from the nucleotide fasta header or the blastx report. Provides a human readable label. NULL for non-exemplars
	('nucleotide_header', 'TEXT'),	# The header in the nucleotide fasta from which the sequence was loaded
	('protein_header', 'TEXT'),	# The header in the protein fasta from which the sequence was loaded
	('note', 'TEXT')			# Useful for user specified information')
)

sequences_index = ('run_id', 'catalog_id', 'locus', 'transcript')
sequences_sql = _create_sql('sequences', sequences_schema, sequences_index)

rpsblast_schema = (
	('run_id', 'INTEGER'),			# Foreign key to runs table, refers to the run_id of the process that populates the database
	('catalog_id', 'VARCHAR(256)'),			# query - Foreign key to catalog.id, could be obtained through join with run table but including it here makes things more robust
	('locus', 'INTEGER'),			# query - Parsed from the header, eg 3 from Locus_3_Transcript_20
	('transcript', 'INTEGER'),			# query - Parsed from the header, eg 20 from Locus_3_Transcript_20
	('hit_id', 'VARCHAR(256)'), 		# subject - accession number, parsed from <Hit_id> field of blast report
	('mask', 'TEXT'),		# mask showing where hits are on query
	('evalue', 'FLOAT'),
	('score', 'FLOAT')
)

rpsblast_index = ('run_id', 'catalog_id', 'locus', 'transcript')
rpsblast_sql = _create_sql('rpsblast', rpsblast_schema, rpsblast_index)

homology_schema = (
	('run_id', 'INTEGER'),			# Foreign key to runs table, refers to the run_id of the process that assigned homology and populated this table
	('component_id', 'INTEGER'),	# The id assigned to the component during graph processing
	('sequence_id', 'INTEGER'),		# Foreign key to sequences table, identifies the sequence assigned to a particular component
)

homology_index = ('run_id', 'component_id')
homology_sql = _create_sql('homology', homology_schema, homology_index)

def _create_db():
	global _db
	_db.executescript(sequences_sql + rpsblast_sql + homology_sql)

def connect(*args, **kwargs):
	"""
	Establish a gobal database connection and set the `execute` function.
	"""
	global _db, execute
	if _db is None:
		path = get_resource('agalma_database')
		exists = os.path.isfile(path)
		if not exists:
			utils.safe_mkdir(os.path.dirname(path))
		_db = sqlite3.connect(path, timeout=60.0, isolation_level=None)
		if not exists:
			_create_db()
		execute = _db.execute
	if len(args):
		return execute(*args, **kwargs)

def disconnect():
	"""
	Close the global database connection, set it to None, and set the
	`execute` function to auto-reconnect.
	"""
	global _db, execute
	if _db is not None:
		_db.close()
	_db = None
	execute = connect

execute = connect


# List of domains to mask, should eventually be put in config file
domains_to_mask = [

'gnl|CDD|144943', 
'gnl|CDD|201208', 
'gnl|CDD|200957', 
'gnl|CDD|197705', 
'gnl|CDD|143167', 
'gnl|CDD|200980', 
'gnl|CDD|200936', 
'gnl|CDD|144972', 
'gnl|CDD|200951', 
'gnl|CDD|206635', 
'gnl|CDD|197520', 
'gnl|CDD|143165', 
'gnl|CDD|29261', 
'gnl|CDD|200936', 
'gnl|CDD|197603', 
'gnl|CDD|201739', 
'gnl|CDD|200931', 
'gnl|CDD|200948', 
'gnl|CDD|200998', 
'gnl|CDD|201332', 
'gnl|CDD|201372', 
'gnl|CDD|201053', 
'gnl|CDD|201004', 
'gnl|CDD|201223', 
'gnl|CDD|200988', 
'gnl|CDD|200930', 
'gnl|CDD|197585', 
'gnl|CDD|197685', 
'gnl|CDD|197533', 
'gnl|CDD|28904', 
'gnl|CDD|201276', 
'gnl|CDD|28904', 
'gnl|CDD|197562', 
'gnl|CDD|28898', 
'gnl|CDD|201143', 
'gnl|CDD|197480'

] 


"""
The domains to mask are specified via the PSSM-Id, but the list that we have
from previous analyses is of the "View PSSM" field. Can search for the "View
PSSM" field at

http://www.ncbi.nlm.nih.gov/Structure/cdd/cdd.shtml

Then get the PSSM-Id in the Statistics dropdown of the result page

"View PSSM"	"PSSM-Id"
pfam01535	144943
pfam00400	201208
pfam00047	200957
smart00407	197705
cd00099	143167
pfam00076	200980
pfam00023	200936
pfam01576	144972
pfam00041	200951
cd00031	206635
smart00112	197520
cd00096	143165
cd00204	29261
pfam00023	200936
smart00248	197603
pfam01344	201739
pfam00018	200931
pfam00038	200948
pfam00096	200998
pfam00595	201332
pfam00651	201372
pfam00169	201053
pfam00105	201004
pfam00435	201223
pfam00084	200988
pfam00017	200930
smart00225	197585
smart00367	197685
smart00135	197533
cd00020	28904
pfam00514	201276
cd00020	28904
smart00185	197562
cd00014	28898
pfam00307	201143
smart00033	197480

"""

SeqRecord = namedtuple('SeqRecord', "id header seq")

def load_seqs(load_id, species, seq_type):
	"""
	Returns a list of SeqRecords from the sequences table for the given a
	load_id.
	
	seq_type can have one of three values:
	
	masked_protein	Protein sequences are written, with promiscuous domains masked
	protein			Protein sequences are written, without any masking
	nucleotide		Nucleotide sequences are written, without any masking
	"""

	species_name = species.name.replace(' ', '_')

	seq_to_select = ''
	if (seq_type == 'masked_protein') or (seq_type == 'protein'):
		seq_to_select = 'protein_seq'
	elif seq_type == 'nucleotide':
		seq_to_select = 'nucleotide_seq'
	else:
		utils.die("{0} is not a valid seq_type".format(seq_type))

	records = list()

	rows = execute("""
		SELECT sequence_id, %s, locus, transcript
		FROM sequences
		WHERE run_id=? AND blast_hit!='';""" % seq_to_select, (load_id,))

	for row in rows:

		sequence_id = row[0]
		sequence_string = row[1]
		locus = row[2]
		transcript = row[3]

		# Mask the sequence if need be
		if seq_type == 'masked_protein':

			# Need to mask promiscuous domains
			mask_sum = []

			rpsrows = execute("""
				SELECT mask, hit_id
				FROM rpsblast
				WHERE run_id=? AND locus=? AND transcript=?;""",
				(load_id, locus, transcript))

			for rpsrow in rpsrows:
				mask = rpsrow[0]
				hit_id = rpsrow[1]
				if len(mask) != len(sequence_string):
					utils.die("sequence at locus {} transcript {} has different length from mask for rps hit {}".format(locus, transcript, hit_id))
				hit_id = rpsrow[1]
				# print "\t", rpsrow
				if hit_id in domains_to_mask:
					# print "\t\tMasking locus {0} transcript {1} for domain {2}".format( locus, transcript, hit_id )

					# Get the mask as a list of boolean integers
					mask = [int(x) for x in mask]

					# Add the mask to the running tally
					if len(mask_sum) < 1:
						mask_sum = mask
					else:
						mask_sum = [sum(item) for item in zip(mask_sum,mask)]

			# Check to see if any mask values are nonzero, if so then mask the sequence
			if sum(mask_sum) > 0:
				masked_seq = []
				for i, m in enumerate(mask_sum):
					if m == 0:
						masked_seq.append(sequence_string[i])
					else:
						masked_seq.append('X')
				sequence_string = ''.join(masked_seq)

		# Trim any stop codons
		sequence_string = re.sub('\*.+', '', sequence_string)

		# Format the sequence record and append it to the list
		header = '{0}@{1}'.format(species_name, sequence_id)
		records.append(SeqRecord(sequence_id, header, sequence_string))

	return records


