#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Estimates the insert size distribution of paired-end Illumina data by assembling a subset
of the data and mapping read pairs to it. The insert size does not include the adapters
added during library preparation.
"""

import os
import numpy as np

from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import wrappers
from biolite import workflows

from biolite.config import get_resource
from biolite.pipeline import IlluminaPipeline

pipe = IlluminaPipeline('insert_size', __doc__)

# Command-line arguments.

pipe.add_argument('subset_size', short='s', type=int, metavar='N',
	default=100000, help="""
	Use N high quality reads for assembly.""")

pipe.add_argument('quality', short='q', type=int, metavar='MIN',
	default=36, help="""
	Filter out reads that have a mean quality < MIN from assembly.""")

pipe.add_argument('stats_subset_size', short='S', type=int, metavar='SUBSET',
	default=10000, help="""
	Use N reads to calculate insert size statistics.""")

pipe.add_argument('stats_quality', short='Q', type=int, metavar='QUALITY',
	default=0, help="""
	Filter out reads that have a mean quality < MIN from statistics.""")

# Pipeline stages.

@pipe.stage
def subset(id, data, subset_size, quality):
	"""Construct subset of high quality reads"""

	subset = []

	for i, _ in enumerate(data[-1]):
		subset.append('%s.subset.%d.fastq' % (id, i+1))

	wrappers.FilterIllumina(
		data[-1],
		subset,
		quality=quality,
		nreads=subset_size)

	ingest('subset')

@pipe.stage
def subset_oases(subset):
	"""Oases assemblies of subset with k in (31,41,51,61)"""
	transcripts = 'subassembly.all.fa'
	workflows.oases_concat_assembly(subset, transcripts,
			kmers=(31,41,51,61), ins_length=get_resource('mean_insert_size'))
	ingest('transcripts')

@pipe.stage
def stats_subset(id, data, stats_subset_size, stats_quality):
	"""Construct subset of reads to base statistics on"""

	stats_subset = []

	for i, _ in enumerate(data[-1]):
		stats_subset.append('%s.stats_subset.%d.fastq' % (id, i+1))

	wrappers.FilterIllumina(
		data[-1],
		stats_subset,
		quality=stats_quality,
		nreads=stats_subset_size)

	ingest('stats_subset')

@pipe.stage
def bowtie(stats_subset, transcripts):
	"""Run bowtie against transcripts"""
	wrappers.Bowtie2Build(transcripts, 'bowtietranscripts')
	bowtie_sam = 'bowtie_map.sam'
	wrappers.Bowtie2(stats_subset, 'bowtietranscripts', bowtie_sam)

	ingest('bowtie_sam')

@pipe.stage
def bowtie_to_bam(bowtie_sam):
	"""Convert bowtie SAM output to sorted, indexed BAM"""
	sorted_bam = workflows.sort_and_index_sam(bowtie_sam)

	ingest('sorted_bam')

@pipe.stage
def estimate_insert(bowtie_sam, data, outdir):
	"""Calculate mean and standard deviation of insert size"""
	insert_hist = os.path.join(outdir, 'insert_hist.txt')
	wrappers.InsertStats(bowtie_sam, insert_hist, get_resource('max_insert_size'))

	finish('data')

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
Field = report.Field
insert_schema = (
	Field('mean', "Mean insert size (bp)", float, '{:.2f}'),
	Field('stddev', "Standard deviation (bp)", float, '{:.2f}'))

class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('insert', 'insert_size.estimate_insert.insert_stats')
		self.lookup('hist_file', 'insert_size.estimate_insert.insert_stats',
																'hist_file')
		# Generators
		self.generator(self.insert_table)
		if self.outdir:
			self.generator(self.insert_histogram)

	def insert_table(self):
		return self.summarize(insert_schema, 'insert')

	def insert_histogram(self):
		"""A histogram of insert sizes."""
		if 'hist_file' in self.data:
			with open(self.data.hist_file, 'r') as f:
				# Load in the histogram, which has a bin per insert size.
				hist = dict(enumerate(map(int, f.readlines())))
			# Generate a histogram that is rebinned to 20 insert sizes
			# per bin.
			imgname = "%d.insert.hist.png" % self.run_id
			props = {
				'title': "Distribution of Insert Sizes",
				'xlabel': "Insert Size (bp)",
				'ylabel': "Frequency"}
			return [self.histogram(imgname, hist, props=props)]

