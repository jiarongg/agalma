#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Applies sampling and length filters to each cluster of homologous sequences.

Uses an all-by-all BLAST alignment of sequences within each cluster to trim
sequence ends not included in any HSP.

Creates multiple sequence alignments for each cluster of homologous sequences
using Macse, a translation aware aligner that accounts for frameshifts and stop
codons; see doi:10.1371/journal.pone.0022594 

"""

import glob
import numpy as np
import os

from collections import defaultdict, namedtuple
from itertools import izip
from operator import attrgetter
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Blast import NCBIXML

import database

from biolite import catalog
from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import workflows
from biolite import wrappers

from biolite.pipeline import Pipeline

pipe = Pipeline('multalign', __doc__)

# Command-line arguments.
pipe.add_argument('homology_id', type=int, default=None, help="""
	The run ID to lookup in the 'homology' table of homologous sequences.""")
pipe.add_argument('min_taxa', type=int, default=4, help="""
	Minimum number of taxa required to retain a cluster.""")
pipe.add_argument('max_sequences', type=int, default=300, help="""
	Maximum number of sequences to allow in a cluster.""")
pipe.add_argument('max_length', type=int, default=10000, help="""
    Maximum length for sequences in a cluster.""")
pipe.add_argument('frameshift', short='f', type=int, default=-40, help="""
	Penalty for frameshift within the sequence.""")
pipe.add_argument('stopcodon', short='s', default=-150, help="""
	Penalty for stop codon not at the end of the sequence.""")
pipe.add_argument('frameshift_cutoff', type=float, default=0.1, help="""
	Remove sequences with more than this proportion of frameshifts.""")
pipe.add_argument('supermatrix', default=False, action='store_true', help="""
	If specified, concatenate all cleaned alignments into a supermatrix with
	one sequence per taxa.""")

# Data struct for passing clusters between stages
Cluster = namedtuple('Cluster', "name size weight fasta")

@pipe.stage
def lookup_homology_id(id, homology_id):
	"""Lookup the run ID for the last homology/treeprune run with the current ID"""
	if not homology_id:
		prev = diagnostics.lookup_last_run(id, 'treeprune')

		if not prev:
			prev = diagnostics.lookup_last_run(id, 'homologize')

		if not prev:
			utils.die("could not find previous run for '%s': use --homologize_id" % id)

		utils.info("using previous '%s' run id '%s'" % (prev.name, prev.run_id))
	
		return {'homology_id': int(prev.run_id)}

@pipe.stage
def refine_clusters(_run_id, homology_id, min_taxa, max_sequences, max_length):
	"""Create a cluster for each homologize component that meets size, sequence length, and composition requirements"""

	clusters = []
	components = defaultdict(list)
	taxa = {}
	nseqs = 0

	utils.safe_mkdir('untrimmed_clusters')

	# Load components from database
	sql = """
		SELECT homology.component_id, homology.sequence_id,
		       sequences.nucleotide_seq, sequences.catalog_id
		  FROM homology, sequences
		 WHERE homology.sequence_id=sequences.sequence_id
		   AND homology.run_id=?;"""

	for row in database.execute(sql, (homology_id,)):
		seq = row[2]
		# Filter out sequences that are too long
		if len(seq) <= max_length:
			comp_id = int(row[0])
			taxa[row[3]] = None
			record = SeqRecord(Seq(seq), name=row[3], id=str(row[1]), description='')
			components[comp_id].append(record)

	# Lookup species names for catalog ids
	for id in taxa:
		record = catalog.select(id)
		if not record or not record.species:
			utils.die("couldn't find species for catalog id '%s'" % id)
		taxa[id] = record.species.replace(' ', '_')

	# Loop over the components and write the fasta files
	for comp_id, records in components.iteritems():

		# Apply filter on cluster size
		size = len(records)

		if size <= max_sequences:

			# Count the number of times a taxon is repeated in the cluster
			# and calculate the cluster weight
			taxa_count = {} 
			weight = 0

			for record in records:
				taxon = taxa[record.name]
				record.id = '%s@%s' % (taxon, record.id)
				taxa_count[taxon] = taxa_count.get(taxon, 0) + 1
				weight += len(record.seq)

			# Weight is an approximation of how many comparisons need to be
			# performed in multiple alignment, for ordering the Macse
			# calls later
			weight = (weight / size) ** size

			# Compute the mean number of repetitions
			taxa_count = taxa_count.values()
			taxa_mean = sum(taxa_count) / float(len(taxa_count))

			# Require at least min_taxa, and that the mean reptitions is less
			# than 5.
			if len(taxa_count) >= min_taxa and taxa_mean < 5:
				name = 'homologs_{0}_{1}'.format(_run_id, comp_id)
				fasta  = os.path.join('untrimmed_clusters', name + '.fa')
				SeqIO.write(records, open(fasta, 'w'), 'fasta')
				clusters.append(Cluster(name, size, weight, [fasta]))
				nseqs += size

	utils.info("found the following taxa for homology id %d:" % homology_id)
	print '\n'.join("  %s (%s)" % (taxon,id) for id,taxon in taxa.iteritems())

	if not clusters:
		utils.die("no clusters passed the filtering criteria")

	diagnostics.log('taxa', str(taxa))
	diagnostics.log('nseqs', nseqs)
			
	ingest('clusters')

	return {'taxa': set(taxa.itervalues())}


@pipe.stage
def trim_sequences(clusters):
	"""Compare homologs within a cluster by all-to-all BLAST and trim sequence ends not included in any HSP"""

	utils.safe_mkdir('trimmed_clusters')
	
	# Loop through each fasta file
	for cluster in clusters:

		fasta = cluster.fasta[-1]
		trimmed_fasta = os.path.join('trimmed_clusters', cluster.name + '.fa')
		cluster.fasta.append(trimmed_fasta)

		# Create blast DB
		dbname = os.path.join(os.getcwd(), "trim_allvall") 
		wrappers.MakeBlastDB('nucl', fasta, dbname)

		# Run tblastx
		report = 'trim_allvall.xml'
		workflows.multiblast('tblastx', fasta, dbname, report, evalue='1e-6')

		# Load original sequences into a dict
		seqs = dict()
		for record in SeqIO.parse(open(fasta), 'fasta'):
			seqs[record.id] = record

		with open(trimmed_fasta, 'w') as f:
			for record in NCBIXML.parse(open(report)):
				seq = seqs[record.query]
				# Make sure the sequence in our dict matches up with the query
				assert len(seq.seq) == record.query_length
				# Initialize list as long as query
				mask = ['0'] * record.query_length
				for align in record.alignments:
					# Skip hit to itself
					if record.query != align.hit_def:
						for hsp in align.hsps:
							# Mask the query range, adjusting the range by -1
							# because BLAST reports 1-based coordinates
							for i in range(hsp.query_start-1, hsp.query_end):
								mask[i] = '1'
				# Apply mask
				mask = ''.join(mask)
				start = mask.find('1')
				end = mask.rfind('1')
				if (start >= 0) and (start < end):
					seq.seq = seq.seq[start:end+1]
				SeqIO.write(seq, f, 'fasta')


@pipe.stage
def align_sequences(clusters, frameshift, stopcodon, outdir):
	"""Align nucleotide sequences within each component using MACSE"""

	utils.safe_mkdir(os.path.join(outdir, 'macse'))

	inputs = []
	outputs = []

	# Sort by descending size to improve load balancing in parallel MACSE
	for cluster in sorted(clusters, key=attrgetter('weight'), reverse=True):
		basename = os.path.join(outdir, 'macse', cluster.name)
		inputs.append(cluster.fasta[-1])
		outputs.append(basename)
		cluster.fasta.append(basename + '_AA.fasta')
		cluster.fasta.append(basename + '_DNA.fasta')

	wrappers.ParallelMacse(inputs, outputs, frameshift, stopcodon)


@pipe.stage
def remove_frameshifts(clusters, frameshift_cutoff, min_taxa):
	"""Remove sequences with too many frameshifts"""

	clusters_keep = []
	nseqs = 0

	# Histograms of frameshifts
	hist_sequences = dict()
	hist_clusters = dict()

	# Count frameshifts across species
	fs_species = dict()
	aa_species = dict()

	clusters_keep = []

	workdir = 'remove_frameshifts'
	utils.safe_mkdir(workdir)

	for cluster in clusters:
		aa_keep = []
		dna_keep = []

		# Count frameshifts across cluster
		fs_cluster = 0
		aa_cluster = 0

		# Count frameshifts in each sequence, and filter out those with too many
		aa_records = list(SeqIO.parse(open(cluster.fasta[-2]), 'fasta'))
		dna_records = SeqIO.parse(open(cluster.fasta[-1]), 'fasta')

		# Truncate sequences after first stop codon
		stop_codons = []
		for aa_record in aa_records:
			stop = aa_record.seq.find('*')
			if stop >= 0:
				stop_codons.append(stop)

		if stop_codons:
			end = min(stop_codons)
			if end == 0:
				utils.info("stop codon at beginning of sequence: dropping cluster '%s'" % cluster.name)
				continue
			else:
				for aa_record in aa_records:
					aa_record.seq = aa_record.seq[:end]

		for aa_record, dna_record in izip(aa_records, dna_records):
			species = aa_record.id.partition('@')[0]

			fs = sum(1 for c in aa_record.seq if c == '!')
			fs_cluster += fs
			fs_species[species] = fs_species.get(species, 0) + fs

			aa = sum(1 for c in aa_record.seq if c != '-')
			aa_cluster += aa
			aa_species[species] = aa_species.get(species, 0) + aa

			if not aa:
				utils.info(
					"all gaps: dropping sequence '%s' in cluster '%s'" %(
					aa_record.description, cluster.name))
				continue

			percent = round(float(fs) / float(aa), 3)
			hist_sequences[percent] = hist_sequences.get(percent, 0) + 1

			if percent > frameshift_cutoff:
				utils.info(
					"too many frameshifts: dropping sequence '%s' in cluster '%s'" % (
					aa_record.description, cluster.name))
			else:
				aa_keep.append(aa_record)
				dna_keep.append(dna_record)

		percent = round(float(fs_cluster) / float(aa_cluster), 3)
		hist_clusters[percent] = hist_clusters.get(percent, 0) + 1

		# Clusters must have at least min_taxa to be meaningful gene trees
		if len(aa_keep) < min_taxa:
			utils.info("fewer than %d taxa: dropping cluster '%s'" % (
					min_taxa,
					cluster.name))
		else: 
			basename = os.path.join(workdir, cluster.name)
			cluster = Cluster(cluster.name, len(aa_keep), cluster.weight, cluster.fasta)
			clusters_keep.append(cluster)
			nseqs += cluster.size
			cluster.fasta.append(basename + '_AA')
			with open(cluster.fasta[-1], 'w') as f:
				for record in aa_keep:
					print >>f, ">%s" % record.description
					print >>f, str(record.seq).replace('!', '-')
			cluster.fasta.append(basename + '_DNA')
			with open(cluster.fasta[-1], 'w') as f:
				for record in dna_keep:
					print >>f, ">%s" % record.description
					print >>f, str(record.seq).replace('!', '-')

	hist_species = {}
	for s in aa_species:
		hist_species[s] = float(fs_species[s]) / float(aa_species[s])

	diagnostics.log('hist_sequences', str(hist_sequences))
	diagnostics.log('hist_clusters', str(hist_clusters))
	diagnostics.log('hist_species', str(hist_species))
	diagnostics.log('nseqs', nseqs)

	return {'clusters': clusters_keep}


@pipe.stage
def cleanup_alignments(clusters, min_taxa, outdir):
	"""Clean up aligned sequences with Gblocks"""

	clusters_keep = []
	nseqs = 0

	protdir = os.path.join(outdir, 'gblocks-prot')
	nucdir = os.path.join(outdir, 'gblocks-nuc')

	utils.safe_mkdir(protdir)
	utils.safe_mkdir(nucdir)

	for cluster in clusters:

		# Skip empty clusters, which were removed in the last stage
		if not cluster.size:
			continue

		aa_name = cluster.fasta[-2]
		dna_name = cluster.fasta[-1]

		# Set Gblocks parameters based on cluster size
		b1 = cluster.size/2 + 1
		b2 = max(b1, int(0.65*cluster.size))

		wrappers.Gblocks(aa_name, b1=b1, b2=b2)
		wrappers.Gblocks(dna_name, t='d', b2=0.65*cluster.size)

		# Parse Gblocks output

		aa_keep = []
		dna_keep = []

		aa_records = SeqIO.parse(open(aa_name + '-gb'), 'fasta')
		dna_records = SeqIO.parse(open(dna_name + '-gb'), 'fasta')

		for aa_record, dna_record in izip(aa_records, dna_records):

			aa_gaps = sum(1 for c in aa_record.seq if c == '-')
			dna_gaps = sum(1 for c in dna_record.seq if c == '-')

			# Remove sequences that are all gaps
			if aa_gaps < len(aa_record.seq) and dna_gaps < len(dna_record.seq):
				aa_keep.append(aa_record)
				dna_keep.append(dna_record)
			else:
				utils.info(
					"dropping sequence '%s' in cluster '%s'" % (
					aa_record.description, cluster.name))

		# Clusters must have at least size min_taxa to be meaningful gene trees
		if len(aa_keep) < min_taxa:
			utils.info("dropping cluster '%s'" % cluster.name)
		else:
			cluster.fasta.append(os.path.join(protdir, cluster.name + '.fa'))
			SeqIO.write(aa_keep, open(cluster.fasta[-1], 'w'), 'fasta')
			cluster.fasta.append(os.path.join(nucdir, cluster.name + '.fa'))
			SeqIO.write(dna_keep, open(cluster.fasta[-1], 'w'), 'fasta')
			clusters_keep.append(cluster)
			nseqs += cluster.size

	diagnostics.log('nseqs', nseqs)

	ingest('protdir', 'nucdir')

	return {'clusters': clusters_keep}


@pipe.stage
def build_supermatrix(supermatrix, taxa, clusters, protdir, nucdir, outdir):
	"""Concatenate multiple alignments together into a supermatrix if the --supermatrix argument was used"""

	if supermatrix:

		nucdir = os.path.join(outdir, 'supermatrix-nuc')
		protdir = os.path.join(outdir, 'supermatrix-prot')

		utils.safe_mkdir(nucdir)
		utils.safe_mkdir(protdir)

		diagnostics.prefix.append('nuc')
		workflows.phylogeny.supermatrix(
						[cluster.fasta[-1] for cluster in clusters],
						taxa, nucdir, 'DNA')
		diagnostics.prefix.pop()

		diagnostics.prefix.append('prot')
		workflows.phylogeny.supermatrix(
						[cluster.fasta[-2] for cluster in clusters],
						taxa, protdir, 'WGA')
		diagnostics.prefix.pop()

	else:
		utils.info("skipping")

	finish('protdir', 'nucdir')


# Report. #
class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.hist_names = ('hist_sequences', 'hist_clusters', 'hist_species')
		for name in self.hist_names:
			self.lookup(name, 'multalign.remove_frameshifts', attribute=name)
		self.lookup('prot_supermatrix',
		            'multalign.build_supermatrix.prot.supermatrix')
		self.lookup('nuc_supermatrix',
		            'multalign.build_supermatrix.nuc.supermatrix')
		# Generators
		self.generator(self.histograms)
		self.generator(self.supermatrix)

	def histograms(self):
		out = []
		for name in self.hist_names:
			if name in self.data:
				hist = eval(self.data[name])
				title = "Distribution of Frameshifts (by %s)" % name.partition('_')[2]
				out.append(self.header(title))
				if len(hist) < 10 or name == 'hist_species':
					# Make a table instead of a plot.
					if name == 'hist_species':
						for k in hist:
							hist[k] *= 100.0
						headers = (
							name.partition('_')[2],
							'Frameshifts (% of AA Sequence)')
						rows = [(k.replace('_', ' '), '%.1f%%' % hist[k]) for k in hist]
					else:
						headers = ('Frameshifts (% of AA Sequence)', 'Frequency')
						rows = [('%g%%' % k, hist[k]) for k in sorted(hist)]
					out += self.table(rows, headers)
				else:
					imgname = "%d.frameshifts.%s.png" % (self.run_id, name)
					props = {
						'xlabel': "Frameshifts (% of AA Sequence)",
						'xticks': [0, len(hist)-1],
						'xticklabels': ['0%', '%.1f%%' % (100.0*max(hist))],
						'ylabel': "Frequency"}
					out.append(self.histogram_categorical(imgname, hist, props=props))
		return out

	def supermatrix(self):
		"""
		Images of the protein/nucleotide supermatrices, ordered by most
		complete taxa, then most complete gene. Grayscale shading indicates the
		completeness of the gene in that column, where black is complete and
		white is incomplete.
		"""
		out = []
		supermatrices = (
			('prot_supermatrix', 'Protein supermatrix'),
			('nuc_supermatrix', 'Nucleotide supermatrix'))
		for name, title in supermatrices:
			if name in self.data:
				imgname = "%d.%s.png" % (self.run_id, name)
				# Load taxa names
				taxa = open(self.data[name]['coverage']).readline().strip().split()
				# With transpose, rows are taxa, columns are genes.
				matrix = np.loadtxt(self.data[name]['coverage'], skiprows=1).transpose()
				# Convert 1.0 to black, 0.0 to white
				matrix *= -1.0
				matrix += 1.0
				# Taxa order.
				taxa_sort = np.argsort(np.sum(matrix, axis=1))
				taxa = [taxa[i].replace('_', ' ') for i in taxa_sort]
				matrix = matrix[taxa_sort,:]
				# Gene order.
				gene_sort = np.argsort(np.sum(matrix, axis=0))
				matrix = matrix[:,gene_sort]
				# Plot matrix as grayscale image.
				props = {
					'title': title,
					'xlabel': 'Genes',
					'xticks': [],
					'yticks': range(len(taxa)),
					'yticklabels': taxa,
					'figsize': (9,3)}
				out.append(self.imageplot(imgname, matrix, props))
		return out

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()


