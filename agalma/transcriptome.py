#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
The entire transcriptome pipeline, through assembly.
"""

import os

from biolite import catalog
from biolite import diagnostics
from biolite import utils

from biolite.pipeline import IlluminaPipeline

# This pipeline is a meta-pipeline that connects the following pipelines:

import preassemble
import assemble
import postassemble
import report

pipe = IlluminaPipeline('transcriptome', __doc__)

### preassemble ###

pipe.import_module(preassemble, start=1)

### assemble ###

pipe.add_argument('assemble_quality', type=int, metavar='MIN',
	default=33, help="""
	Filter out reads that have a mean quality < MIN before assembly.""")

pipe.add_argument('assemble_subsets', type=int, nargs='+', metavar='N',
	default=None, help="""
	A list of cumulative subsets of N reads to assemble.""")

@pipe.stage
def connector1(assemble_quality, assemble_subsets):
	"""[connector between "preassemble" and "assemble"]"""
	return {
		'quality': assemble_quality,
		'subsets': assemble_subsets}

pipe.import_module(assemble, ['kmers', 'min_length'], start=1)

pipe.import_module(postassemble, [], start=1)

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()
	# Generate report in directory 'ID.species.report'.
	print "Generating HTML report..."
	id = pipe.get('id')
	record = catalog.select(id)
	outdir = utils.safe_str('%s.%s.report' % (id, str(record.species)))
	report.report_runs(id, outdir, run_ids=[pipe.get('_run_id')], verbose=True)
	print "Zipping HTML report..."
	utils.zipdir(outdir)
	print "Report is in '%s.zip'" % outdir

