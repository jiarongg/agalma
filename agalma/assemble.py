#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Assembles reads into transcripts, processes the assembly, and generates 
assembly diagnostics. Read pairs are first filtered at a more stringent 
mean quality threshold. Assemblies are then performed over a range of 
data subset sizes, which provides an indication of how sequencing effort 
impacts assembly results.
"""

import os
import shutil
import numpy as np

from collections import defaultdict, OrderedDict
from Bio import SeqIO

from biolite import config
from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import workflows
from biolite import wrappers

from biolite.pipeline import IlluminaPipeline

pipe = IlluminaPipeline('assemble', __doc__)

# Command-line arguments.

pipe.add_argument('quality', short='q', type=int, metavar='MIN',
	default=33, help="""
	Filter out reads that have a mean quality < MIN.""")

pipe.add_argument('kmers', short='k', type=int, nargs='+', metavar='K',
	default=(31, 35, 41, 47, 53), help="""
	A list of kmer sizes for Oases-M assembly.""")

pipe.add_argument('subsets', short='s', type=int, nargs='+', metavar='N',
	default=None, help="""
	A list of cumulative subsets of N reads to assemble.""")

pipe.add_argument('min_length', short='l', type=int, metavar='L',
	default=300, help="""
	Only keep transcripts longer than L bases.""")

# Pipeline stages.

@pipe.stage
def quality_filter(id, data, quality, subsets):
	"""Filter out low-quality reads"""

	hiqual = []

	for i,_ in enumerate(data[-1]):
		hiqual.append(os.path.abspath(id + '.hiqual.%d.fastq' % (i+1)))

	if len(data[-1]) == 1:
		# Single end data probably needs adapter and base content filtering.
		wrappers.FilterIllumina(data[-1], hiqual, quality=quality)
	else:
		# Paired end probably ran through earlier agalma pipelines, so don't
		# redo the adapater and base content filtering, but do reformat the
		# separator to '/' from CASAVA 1.6
		wrappers.FilterIllumina(
					data[-1], hiqual, quality=quality,
					adapters=False, bases=False, sep='/')

	# Parse the line count from the output.
	entity = "%s.filter_illumina" % diagnostics.get_entity()
	nreads = int(diagnostics.local_lookup(entity)['pairs_kept'])

	# Find the max subset that is less than the full set of reads.
	if subsets:
		subsets = map(int, subsets)
		subsets = filter(lambda x: x < nreads, subsets)
		subsets.append(nreads)
	else:
		subsets = [nreads]

	diagnostics.log('subsets', subsets)

	data.append(hiqual)

	assemblies = []
	assemblies_full = []

	ingest('subsets', 'assemblies', 'assemblies_full')

@pipe.stage
def oases_merge(assemblies, assemblies_full, data, subsets, kmers, min_length, outdir):
	"""Oases-M assemblies of cumulative subsets of reads"""
	for n in subsets:
		assembly = 'assembly_%d_oases-m' % n
		utils.safe_mkdir(assembly)
		diagnostics.prefix.append(str(n))
		# Accumulate more reads for each cumulative subset.
		if (n != subsets[-1]):
			subset = []
			for i,d in enumerate(data[-1]):
				subset.append('subset.%d.fastq' % (i+1))
				utils.head_to_file(d, subset[-1], 4*n, 'w')
		else:
			subset = data[-1]
		# Assemble, sweeping over kmer sizes.
		contigs = os.path.join(outdir, assembly + '.fa')
		workflows.oases_merge_assembly(
			subset, contigs, 31, kmers, min_length, workdir=assembly)
		assemblies.append(contigs)
		diagnostics.log_path(contigs)
		diagnostics.prefix.pop()

	assemblies_full.append(contigs)

@pipe.stage
def trinity(assemblies, assemblies_full, data, subsets, min_length, outdir):
	"""Trinity assemblies of cumulative subsets of reads"""
	subset1 = 'subset.1.fastq'
	subset2 = 'subset.2.fastq'
	for n in subsets:
		assembly = 'assembly_%d_trinity' % n
		utils.safe_mkdir(assembly)
		diagnostics.prefix.append(str(n))
		# Accumulate more reads for each cumulative subset.
		if (n != subsets[-1]):
			subset = []
			for i,d in enumerate(data[-1]):
				subset.append('subset.%d.fastq' % (i+1))
				utils.head_to_file(d, subset[-1], 4*n, 'w')
		else:
			subset = data[-1]
		contigs = os.path.join(outdir, assembly + '.fa')
		workflows.trinity_assembly(contigs, subset, assembly, min_length)
		assemblies.append(contigs)
		diagnostics.log_path(contigs)
		diagnostics.prefix.pop()

	assemblies_full.append(contigs)

@pipe.stage
def sync(data, assemblies, assemblies_full):
	"""Sync up after all assemblies are finished, and output the list of assemblies"""
	finish('data', 'assemblies', 'assemblies_full')

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
Field = report.Field
filter_schema = (
	Field('pairs', "Read pairs examined", int, '{:,d}'),
	Field('pairs_kept', "Read pairs kept", int, '{:,d}'),
	Field('percent_kept', "Percent kept", float, '{:.1f}%'),
	Field('quality', "Illumina quality threshold", int, '{:,d}'),
	Field('reject_adapters', "Adapter fails", int, '{:,d}'),
	Field('reject_quality', "Quality fails", int, '{:,d}'),
	Field('reject_content', "Base composition fails", int, '{:,d}'))
coverage_schema = (
	Field('pairs', 'Read pairs', int, '{:,d}'),
	Field('mapped', 'Pairs mapped', float, '{:.1f}%'),
	Field('discordant', 'Pairs discordant', float, '{:.1f}%'),
	Field('reads_mapped', 'Unpaired reads mapped', float, '{:.1f}%'))

class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('filter', 'assemble.quality_filter.filter_illumina')
		self.percent('filter', 'percent_kept', 'pairs_kept', 'pairs')
		# Generators
		self.generator(self.filter_table)
		# Backward compatability for >=0.3.0 runs, which can be detected by
		# the 'assembly.' entity.
		if 'assemble.nr_annotate' in diagnostics.lookup_entities(self.run_id):
			self.lookup('assembly_paths', diagnostics.EXIT, 'assembly_paths')
			self.lookup('assemblies', diagnostics.EXIT, 'assemblies')
			self.str2list('assemblies')
			self.lookup('assemblies_full', diagnostics.EXIT, 'assemblies_full')
			self.str2list('assemblies_full')
			if 'assemblies_full' in self.data:
				self.data.best_coverage = dict([(x[0],0) for x in coverage_schema])
				self.data.best_coverage['discordant'] = 100
				self.data.coverage_stats = list()
				self.load_coverage_stats()
			self.generator(self.load_assemblies)
			self.generator(self.assemblies)

	def filter_table(self):
		if 'filter' in self.data:
			# Pull the quality treshold out of the command arguments.
			self.data.filter['quality'] = self.extract_arg(
								'assemble.quality_filter.filter_illumina', '-q')
			html = [self.header("Illumina Filtering")]
			html += self.summarize(filter_schema, 'filter')
			return html

	### Load functions ###

	def load_hist(self, assembly, values, hists):
		try:
			with open(values['path'], 'r') as f:
				hists.append(np.loadtxt(f))
		except IOError:
			utils.info("could not open histogram for assembly '%s', run '%d'" \
													% (assembly, self.run_id))
			hists.append(None)
		except KeyError:
			utils.info("missing histogram entry for assembly '%s', run '%d'" \
													% (assembly, self.run_id))

	def load_coverage(self, assembly, values, coverages, rpkms):
		"""
		Load coverage table into an array. Fields are:
		0      | 1      | 2        |
		contig | length | coverage |
		"""
		try:
			with open(values['path'], 'r') as f:
				table = np.loadtxt(f, usecols=(1,2))
		except IOError:
			utils.info("could not find histogram for assembly '%s', run '%d'" \
													% (assembly, self.run_id))
			coverages.append(None)
			return

		# Normalize coverage by number of bases in the contig.
		norm = table[:,1] / table[:,0]

		# Sort by coverage, descending.
		norm = np.sort(norm)[::-1]

		# Compute cumulative sum of coverage.
		norm = norm.cumsum()

		# Scale to percentage.
		#scale = 100.0 / norm[-1]
		#norm *= scale

		coverages.append(norm)

		# Calculate RPKM as:
		# (1e9 * coverage) / (len * coverage_sum)
		coverage_norm = 1.0 / table[:,1].sum()
		table[:,1] *= 1e9 * coverage_norm
		table[:,1] /= table[:,0]

		rpkms.append(table)

	def load_coverage_stats(self):
		for assembly in self.data.assemblies_full:
			best = self.data.best_coverage
			stats = self.data.coverage_stats
			stats.append(dict())
			values = diagnostics.lookup(self.run_id,
								"assemble.coverage.%s.bowtie2" % assembly)
			if 'npairs' in values:
				stats[-1]['pairs'] = values['npairs']
				best['pairs'] = max(best['pairs'], stats[-1]['pairs'])
				norm = 100.0 / float(values['npairs'])
				if 'nconcord1' in values and 'nconcord2' in values:
					stats[-1]['mapped'] = norm * \
							(int(values['nconcord1']) + int(values['nconcord2']))
					best['mapped'] = max(best['mapped'], stats[-1]['mapped'])
				if 'ndiscord1' in values:
					stats[-1]['discordant'] = norm*int(values['ndiscord1'])
					best['discordant'] = min(best['discordant'], stats[-1]['discordant'])
				if 'nunpaired0' in values:
					stats[-1]['reads_mapped'] = \
										100.0 - 0.5*norm*int(values['nunpaired0'])
					best['reads_mapped'] = max(best['reads_mapped'], stats[-1]['reads_mapped'])

	def split_assembly(self, assembly):
		tokens = assembly.split('_')
		if tokens[0] != 'assembly' or len(tokens) < 3:
			utils.info("couldn't parse assembly name for '%s', run %d" % \
														(self.run_id, assembly))
		return int(tokens[1]), ' '.join(tokens[2:])

	def load_assemblies(self):
		if 'assemblies' in self.data:
			self.data.counts = defaultdict(dict)
			self.data.means = defaultdict(dict)
			self.data.n50s = defaultdict(dict)
			self.data.nr_counts = defaultdict(dict)
			self.data.nr_means = defaultdict(dict)
			self.data.nr_n50s = defaultdict(dict)
			self.data.subsets = set()
			for assembly in self.data.assemblies:
				subset, method = self.split_assembly(assembly)
				self.data.subsets.add(subset)
				values = diagnostics.lookup(self.run_id,
							"assemble.contig_stats.%s.contig_stats" % assembly)
				if values:
					self.data.counts[method][subset] = int(values['count'])
					self.data.means[method][subset] = float(values['mean'])
					self.data.n50s[method][subset] = int(values['n50'])
				values = diagnostics.lookup(self.run_id,
							"assemble.nr_annotate.%s.contig_stats" % assembly)
				if values:
					self.data.nr_counts[method][subset] = int(values['count'])
					self.data.nr_means[method][subset] = float(values['mean'])
					self.data.nr_n50s[method][subset] = int(values['n50'])
			self.data.subsets = sorted(self.data.subsets)

		if 'assemblies_full' in self.data:
			self.data.clean_hists = list()
			self.data.swissprot_hists = list()
			self.data.coverages = list()
			self.data.nr_coverages = list()
			self.data.rpkms = list()
			self.data.nr_rpkms = list()
			for assembly in self.data.assemblies_full:
				values = diagnostics.lookup(self.run_id,
							"assemble.contig_stats.%s.contig_stats" % assembly)
				if values:
					self.load_hist(assembly, values, self.data.clean_hists)
				values = diagnostics.lookup(self.run_id,
							"assemble.nr_annotate.%s.contig_stats" % assembly)
				if values:
					self.load_hist(assembly, values, self.data.swissprot_hists)
				values = diagnostics.lookup(self.run_id,
											"assemble.coverage.%s" % assembly)
				if values:
					self.load_coverage(assembly, values, self.data.coverages,
															self.data.rpkms)
				values = diagnostics.lookup(self.run_id,
					"assemble.nr_annotate.%s.filter_coverage_table" % assembly)
				if values:
					self.load_coverage(assembly, values, self.data.nr_coverages,
															self.data.nr_rpkms)

	### Plots ###

	def contig_histogram(self, assembly, clean_hist, swissprot_hist):
		html = [
			"<p>Number of exemplar transcripts in full assembly with blastx hits: {:,d}</p>".format(int(swissprot_hist[:,1].sum()))]
		imgname = "%d.%s.png" % (self.run_id, assembly)
		props = {
			'title': "Exemplar Transcripts - Histogram of Lengths (Full Assembly)",
			'xlabel': "Exemplar Transcript Length (bp)",
			'ylabel': "Frequency"}
			#'yscale': 'log'}
		html.append(
			self.histogram_overlay(imgname,
				[clean_hist, swissprot_hist],
				labels=["Exemplar transcripts", "Exemplar transcripts with blastx hit"],
				props=props))
		return html

	def contig_plot(self, name, title, ylabel):
		if self.check(name):
			data = self.data[name]
			data_bx = self.data['nr_' + name]
			subsets = self.data.subsets
			methods = utils.sorted_alphanum(data.iterkeys())
			y = dict()
			for method in methods:
				y[method] = map(data[method].get, subsets)
			y_bx = dict()
			for method in methods:
				y_bx[method] = map(data_bx[method].get, subsets)
			ymax = max(
					max(map(max, y.itervalues())),
					max(map(max, y_bx.itervalues())))
			# Draw %-% plot.
			imgname = "%d.assemble.%s.png" % (self.run_id, name)
			props = {
				'title': title,
				'xlabel': "Subset size (Number of Read Pairs)",
				'ylabel': ylabel,
				'xticks': [0] + subsets,
				'xlim': [0, 1.1*max(subsets)],
				'ylim': [0, 1.1*ymax],
				'marker': 's',
				'mfc': 'None'}
			plots = [(subsets, y[method], method) for method in methods]
			plots += [(subsets, y_bx[method], method + ' (with blastx hits)') for method in methods]
			return [self.multilineplot(imgname, plots, props)]

	def coverage_plot(self, assembly, cov, nr_cov):
		imgname = "%d.%s.coverage.png" % (self.run_id, assembly)
		props = {
			'title': "Read Coverage Across Exemplar Transcripts",
			'xlabel': "Exemplar Transcripts, ordered from highest to lowest representation",
			'ylabel': "Cumulative coverage (reads-per-base)"}
		plots = (
			(np.arange(cov.size), cov, "all exemplar transcripts"),
			(np.arange(nr_cov.size), nr_cov, "exemplar transcripts with blastx hits"))
		return [self.multilineplot(imgname, plots, props)]

	### Table ###

	def assembly_files(self, assembly):
		links = []
		files = (
			('annotated transcripts', "assemble.nr_annotate.%s.annotated"),
			('rrna', "assemble.clean_rrna.%s"),
			('vectors', "assemble.clean_univec.%s"))
		for label, entity in files:
			src = diagnostics.lookup(self.run_id, entity % assembly).get('path')
			if src and os.path.exists(src):
				dst = '%d.%s' % (self.run_id, os.path.basename(src))
				shutil.copy(src, os.path.join(self.outdir, dst))
				links.append("[<a href=\"%s\">%s</a>]" % (dst, label))
		if links:
			return ', '.join(links)
		else:
			return '-'

	def assembly_table(self):
		html = [
			self.header("Assemblies"),
			"<table><tr>",
			"<th>Method</th><th>Read pairs</th>",
			"<th>Genes</th><th>Mean Length (bp)</th><th>N50 Length (bp)</th>",
			"<th>Links to assembly files</th>",
			"</tr>"]
		for assembly in self.data.assemblies:
			subset, method = self.split_assembly(assembly)
			html.append("<tr>")
			cell = "<td>{}</td>"
			rcell = "<td class=\"right\">{}</td>"
			html.append(cell.format(method))
			html.append(rcell.format('{:,}'.format(int(subset))))
			for stat in ('counts', 'means', 'n50s'):
				try:
					html.append(rcell.format(self.data[stat][method][subset]))
				except KeyError:
					html.append(rcell.format('-'))
			if assembly in self.data.assemblies_full:
				html.append(cell.format(self.assembly_files(assembly)))
			else:
				html.append(cell.format('-'))
			html.append("</tr>")
		html.append("</table>")
		return html

	### Assembly overview ###
	def assemblies(self):
		html = []
		# Table
		if self.check('assemblies', 'assemblies_full'):
			html += self.assembly_table()
			for i, assembly in enumerate(self.data.assemblies_full):
				subset, method = self.split_assembly(assembly)
				html.append(self.header(method, level=1))
				html += self.summarize(coverage_schema, 'coverage_stats', i)
				# Contig histogram
				if self.check('clean_hists', 'swissprot_hists'):
					html += self.contig_histogram(
								assembly,
								self.data.clean_hists[i],
								self.data.swissprot_hists[i])
				# Coverage plot
				if self.check('coverages', 'nr_coverages'):
					html += self.coverage_plot(
								assembly,
								self.data.coverages[i],
								self.data.nr_coverages[i])
			# Contig plots
			# Only run if we have subsets to compare
			if len(self.data.assemblies) > len(self.data.assemblies_full):
				html.append(self.header("Subset Comparison", level=1))
				html += self.contig_plot(
							'counts',
							"Exemplar Transcripts - Genes",
							"Number of Exemplar Transcripts")
				html += self.contig_plot(
							'means',
							"Exemplar Transcripts - Mean Length",
							"Length (bp)")
				html += self.contig_plot(
							'n50s',
							"Exemplar Transcripts - N50 Length",
							"Length (bp)")
		return html

