#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Assembles and identifies ribosomal RNA (rRNA) sequences, removes read pairs
that map to these rRNA sequences, and provides a variety of diagnostics about
rRNA. A single exemplar sequence is presented for each type of rRNA that is
found, but rRNA read pairs are excluded by mapping to a large set of rRNA
transcripts that are derived from multiple assemblies over a range of data
subset sizes.
"""

import os
import re
import textwrap
import numpy as np

from Bio import SeqIO
from string import Template
from collections import defaultdict, OrderedDict

from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import wrappers
from biolite import workflows

from biolite.config import get_resource
from biolite.pipeline import IlluminaPipeline

pipe = IlluminaPipeline('remove_rrna', __doc__)

# Command-line arguments.

pipe.add_argument('keep', short='K', type=bool,
	default=False, help="""
	Keep rRNA instead of excluding it.""")

pipe.add_argument('fasta_target', short='t', type=str,
	default='rrna', help="""
	Resource fasta file to use.""")

# Pipeline stages.

@pipe.stage
def subset_assemblies(id, data):
	"""Assemble subsets of reads"""
	subset_sizes = [500,1000,2500,5000,10000,25000,50000,100000,250000,500000,1000000]
	
	# Open a file for the concatenated assemblies
	transcripts = 'transcripts.all.fasta'
	sub_file = open(transcripts,'w')
	
	subset = []
	for subset_size in subset_sizes:
		diagnostics.prefix.append(str(subset_size))

		this_subset = []

		for i in range(len(data[-1])):
			# Create subsets of reads
			this_subset.append(id + '.subset.{0}.{1}.fastq'.format(subset_size, i+1))

		subset.append(this_subset)

		wrappers.FilterIllumina(data[-1], this_subset, quality=36, nreads=subset_size)
		
		# Assemble the subset
		transcripts_sub_raw = 'subassembly.{0}.all.fa'.format(subset_size)
		workflows.oases_concat_assembly(this_subset, transcripts_sub_raw, kmers=[61])
		
		# Extract an exemplar transcript for each locus
		transcripts_sub = 'subassembly.{0}.exemplars.fasta'.format(subset_size)
		workflows.extract_oases_exemplars(transcripts_sub_raw, transcripts_sub)

		# Loop through the subassembly, append the subset size to the locus so that loci are 
		# unique across assemblies, and add them to cat_subassemblies
		f = open(transcripts_sub, 'r')
		for record in SeqIO.parse(f, 'fasta'):
			header = record.id
			
			# remove text in other header fields, otherwise the BioPython parsing leads to 
			# duplicated headers
			record.name = ''
			record.description = ''
			
			# Reformat the header in subset.locus format
			record.id = header.replace('Locus_', 'Locus_{0}.'.format(subset_size))
			
			SeqIO.write(record, sub_file, 'fasta')

		diagnostics.prefix.pop()

	ingest('transcripts', 'data')

@pipe.stage
def blast_rrna(transcripts, fasta_target):
	"""BLAST transcripts against known rRNA database"""
	
	# Build the blast database each time from the fasta. This occurs a little
	# bit of overhead, but ensure that it is up to date and also that it is
	# being created somewhere that the users has write access
	 
	dbname = "rRNA_blast"
	
	# Blastn appends the current working directory to the search path for blast
	# databases. These databases are separated by colons, so if there is a
	# colon in the cwd then it becomes truncated and the database cannot be
	# found. As a workaround, the absolute path to the database can be used
	# even though it is in the cwd.

	dbname = os.path.join(os.getcwd(), dbname) 

	wrappers.MakeBlastDB('nucl', get_resource(fasta_target + "_fasta"), dbname)
	
	rrna_blast = 'rrna_blast.xml'
	wrappers.Blastn(transcripts, dbname, rrna_blast, evalue=0.0001)
	ingest('rrna_blast')

@pipe.stage
def parse_blast_rrna(transcripts, rrna_blast, outdir, fasta_target):
	"""Parse rRNA BLAST output for exemplar rRNA sequences"""
	# Parse out the exemplar hits, unpacking the locus name from the header

	rrna_names, hits = workflows.rrna_blast_hits(rrna_blast,
												workflows.unpack_oases_header)

	utils.info("Total of %d matches against ribosomal RNA." % len(rrna_names))

	# Create a dictionary of all rRNA sequences from the assembly.
	rrna_seqs = dict()
	with open(transcripts, 'rU') as f:
		for record in SeqIO.parse(f, 'fasta'):
			if record.id in rrna_names:
				rrna_seqs[record.id] = record

	# Build a new dictionary of rRNA sequences, annotating with rRNA
	# type, and taking reverse complement when needed.
	annotated_rrna_seqs = dict()
	for hit in hits.itervalues():
		record = rrna_seqs[hit.query]
		record.name = ''
		record.description = ''
		record.id = "%s|%s" % (hit.gene, hit.query)
		if hit.orient == -1:
			record.seq = record.seq.reverse_complement()
		annotated_rrna_seqs[record.id] = record

	# Write all rRNA sequences to a file.
	rrna_all = 'rrna_all.fa'
	with open(rrna_all, 'w') as f:
		SeqIO.write(annotated_rrna_seqs.itervalues(), f, 'fasta')

	# Organize the sequences in a dictionary keyed by rRNA type with value list
	# of sequences.
	rrna_genes = defaultdict(list)
	for key in annotated_rrna_seqs.keys():
		gene_name = key.partition("|")[0]
		rrna_genes[ gene_name ].append( annotated_rrna_seqs [ key ] )

	# Exclude all but the longest sequence for each gene for each assembly
	
	fe = open('rrna_one_per_assembly_per_gene.fa', 'w') 
	for gene_name in rrna_genes.keys():
		# Example header:
		# >18s|Locus_2500.6_Transcript_1/1_Confidence_1.000_Length_222

		# Need to preserve order, but retain only one sequence for each
		# subset size. The subset size is the integer following "Locus_" and
		# preceding the locus number specified after the dot.

		elements = dict()
	
		# The locus ID in the contig was modified so that it contains the
		# subset size, which we will extract with this regex:
		header_pattern = re.compile(r'Locus_(\d+)\.')
		# Loop over the records for the gene	
		for record in rrna_genes[gene_name]:
			match = header_pattern.search(record.id)
			subset_size = int(match.group(1))
			previous = elements.get(subset_size, None)
			if not previous or len(previous.seq) < len(record.seq):
				elements[subset_size] = record

		# Replace the full list for the gene with the list that includes no more than one 
		# sequence per assembly subset size

		rrna_genes[gene_name] = [elements[k] for k in sorted(elements.keys())]

		SeqIO.write(rrna_genes[gene_name], fe, 'fasta') 

	fe.close()

	# Create a dictionary with a list of sequence lengths for each rRNA gene
	# in the reference set with key gene name and value list of lengths
	rrna_gene_lengths = defaultdict(list)

	with open(get_resource(fasta_target + "_fasta"), 'rU') as f:
		for record in SeqIO.parse(f, 'fasta'):
			gene_name = record.id.split("|")[0]
			rrna_gene_lengths[gene_name].append(len(record.seq))

	rrna_gene_names = sorted(map(str, rrna_gene_lengths.keys()))
	diagnostics.log('rrna_gene_names', rrna_gene_names)

	# Identify the single exemplar to retain for each gene
	rrna_exemplar_seqs = dict()
	for gene_name in rrna_gene_names:
		print "Selecting an exemplar for {0}".format(gene_name)

		# Find the longest sequence for this gene in the reference set
		longest_ref = max(rrna_gene_lengths[gene_name])

		# Calculate a length limit based on the reference set
		length_limit = longest_ref * 1.25

		print "\tlength_limit set to {0}".format(length_limit)

		exemplar = None

		# rrna_gene_names is a list of the genes that were in the rRNA.fasta file
		# Also need a list of the the gene names that were found in the data

		found_rrna_gene_names = rrna_genes.keys()

		# If the current gene_name wasn't found in the data, continue
		if not gene_name in found_rrna_gene_names:
			print "\tnot found in the assembly, skipping"
			continue

		# Use list comprehension to get a list of sequence lengths from the list of 
		# sequence records stored in the dictionary; use this to find the length of
		# the longest sequence and compare it to length_limit

		if max( [ len(x.seq) for x in rrna_genes[gene_name] ] ) <= length_limit:
			# If no sequence exceeds the length_limit, then use the longest sequence
			print "\tNo sequences exceed length_limit, selecting the longest sequence"

			for record in rrna_genes[gene_name]:
				if not exemplar or len(record.seq) > len(exemplar.seq):
					exemplar = record
			
		else:
			# If there is a sequence that exceeds length_limit, then use either the 
			# sequence that has the first local maximum or that precedes the 
			# first sequence to exceed length_limit
			print "\tOne or more sequences exceeds length_limit"
			i = 0
			for record in rrna_genes[gene_name]:
				i = i + 1
				if len( record.seq ) > len( rrna_genes[gene_name][i] ):
					exemplar = record
					print "\tSelected the first local maximim"
					break
				elif len( rrna_genes[gene_name][i] ) > length_limit:
					exemplar = record
					print "\tSelected the sequence that precedes the first sequence to exceed length_limit"
					break

		# Record the exemplar sequence
		print "\t{0} selected for gene {1}".format( exemplar.id, gene_name )
		rrna_exemplar_seqs[gene_name] = exemplar

	# Write exemplar rRNA sequences to a file.
	rrna_exemplars = os.path.join(outdir, 'rrna_exemplars.fasta')
	with open(rrna_exemplars, 'w') as f:
		SeqIO.write(rrna_exemplar_seqs.itervalues(), f, 'fasta')

	diagnostics.log_path(rrna_exemplars)

	ingest('rrna_all', 'rrna_exemplars', 'hits', 'rrna_gene_names', 'rrna_exemplar_seqs')

@pipe.stage
def blast_nt(rrna_exemplars, rrna_exemplar_seqs):
	"""BLAST rRNA exemplars against nt database"""
	if len(rrna_exemplar_seqs):
		nt_blast = 'nt_blast.xml'
		wrappers.Blastn(rrna_exemplars, get_resource('nt_blastdb'), nt_blast)
	else:
		nt_blast = None
		utils.info("no rRNA exemplars were found... skipping")

	ingest('nt_blast')

@pipe.stage
def parse_blast_nt(nt_blast, outdir):
	"""Parse nt BLAST output"""
	if nt_blast:
		nt_summary = os.path.join(outdir, 'nt_summary.txt')
		with open(nt_summary, 'w') as f:
			print >>f, "query\thit_rank\tevalue\thit"
			# Get a list of all hits, limited to 10 per query.
			for hit in workflows.blast_hits(nt_blast, nlimit=10):
				print >>f, "%s\t%d\t%g\t%s" % (
									hit.query, hit.rank, hit.evalue, hit.title)
		diagnostics.log_path(nt_summary)
	else:
		utils.info("no rRNA exemplars were found... skipping")

@pipe.stage
def bowtie(data, rrna_all):
	"""Run bowtie against rRNA transcripts"""
	wrappers.Bowtie2Build(rrna_all, 'bowtietranscripts')
	bowtie_sam = 'bowtie_map.sam'
	wrappers.Bowtie2(data[-1], 'bowtietranscripts', bowtie_sam, sensitive=False, all=False)

	ingest('bowtie_sam')

@pipe.stage
def bowtie_to_bam(bowtie_sam):
	"""Convert bowtie SAM output to sorted, indexed BAM"""
	sorted_bam = workflows.sort_and_index_sam(bowtie_sam)

	ingest('sorted_bam')

@pipe.stage
def bam_pileup(sorted_bam, rrna_exemplars, outdir):
	"""Pile-up BAM file against transcripts"""
	pileup = os.path.join(outdir, 'remove_rrna.pileup.out')
	#exemplar_bam = 'rrna_exemplars.bam'
	#gene_name = record.id.split("|")[0]
	wrappers.SamPileup(rrna_exemplars, sorted_bam, pileup)

	diagnostics.log_path(pileup)

@pipe.stage
def bam_extract_ids(hits, sorted_bam, rrna_gene_names):
	"""Extract IDs for sequences aligning to exemplars"""
	extracted_sam = 'extracted_{0}.sam'
	# Setup a dictionary of lists keyed by rrna genes.
	queries = dict([(gene, list()) for gene in rrna_gene_names])
	# Populate the dictionary with query names from the exemplar hits.
	for hit in hits.itervalues():
		queries[hit.gene].append('|'.join((hit.gene, hit.query)))
	# Perform a samtools 'view' for each gene.
	genes_used = list()
	for gene, querylist in queries.iteritems():
		diagnostics.prefix.append(gene)
		diagnostics.log('exemplar_count', len(querylist))
		if querylist:
			genes_used.append(gene)
			wrappers.SamView(sorted_bam, querylist, extracted_sam.format(gene))
		diagnostics.prefix.pop()

	ingest('extracted_sam', 'genes_used')

@pipe.stage
def exclude_ids(extracted_sam, genes_used, id, data, outdir, gzip):
	"""Exclude extracted rRNA sequences"""

	excluded = []

	for i in range(len(data[-1])):

		excluded.append(os.path.join(outdir, id + '.%d.norrna.fastq' % (i+1)))

		if gzip:
			excluded[-1] += ".gz"

	wrappers.Exclude(
			[extracted_sam.format(gene) for gene in genes_used],
			data[-1], excluded)

	data.append(excluded)

	finish('data')

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
Field = report.Field
exclude_schema = (
	Field('pairs', "Read pairs examined", int, '{:,d}'),
	Field('pairs_kept', "Read pairs kept", int, '{:,d}'),
	Field('percent_kept', "Percent kept", float, '{:.1f}%'))
rrna_coverage_schema = (
	Field('mean_cov', "Mean coverage", float, '{:.1f}'),
	Field('median_cov', "Median coverage", int, '{:,d}'),
	Field('min_cov', "Min coverage", int, '{:,d}'),
	Field('max_cov', "Max coverage", int, '{:,d}'))

class Report(report.BaseReport):
	rrna_summary = Template("""
		<p><b>$name:</b>$img</p>
		$coverage_table<br/><br/>
		<pre>
		  $seq
		</pre>
		Top NCBI nt hit: <a href="$url">$desc</a> (E-value: $evalue)""")

	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('blast_nt', 'remove_rrna.parse_blast_nt', 'path')
		self.lookup('blast_rrna', 'remove_rrna.parse_blast_rrna', 'path')
		self.lookup('pileup', 'remove_rrna.bam_pileup', 'path')
		self.lookup('exclude', 'remove_rrna.exclude_ids.exclude')
		self.lookup('rrna_gene_names', 'remove_rrna.parse_blast_rrna', 'rrna_gene_names')
		self.str2list('rrna_gene_names')
		self.percent('exclude', 'percent_kept', 'pairs_kept', 'pairs')
		# Generators
		self.generator(self.exclude_table)
		self.generator(self.rrna_summaries)

	def exclude_table(self):
		return self.summarize(exclude_schema, 'exclude')

	def rrna_summaries(self):
		if self.check('rrna_gene_names', 'blast_nt', 'blast_rrna', 'pileup', 'exclude'):
			with open(self.data.blast_nt, 'r') as f:
				nt = self.rrna_nt(f)
			with open(self.data.blast_rrna, 'r') as f:
				(records, piles) = self.rrna_exemplars(f, nt)
			with open(self.data.pileup, 'r') as f:
				self.rrna_pileup(f, piles)
			histograms = self.rrna_histograms(piles)
			exclude = self.data.exclude

			# Parse exclude stats.
			for key, val in exclude.items():
				for rrna in self.data.rrna_gene_names:
					if rrna in key:
						exclude[rrna] = int(val)

			# Build HTML summaries for each rRNA type.
			html = list()

			for rrna in self.data.rrna_gene_names:
				if rrna in records:
					percent = 100.0 * float(exclude[rrna]) / float(exclude['pairs'])
					html.append(self.header("{} / {:,d} target(s) / {:,d} pairs removed ({:.02f}%)").format(
									rrna, len(records[rrna]), exclude[rrna], percent))
					for record in records[rrna]:
						name = record['name']
						coverage = piles.get(name)
						if coverage:
							self.data[name] = {
								'min_cov': min(coverage),
								'max_cov': max(coverage),
								'mean_cov': sum(coverage)/float(len(coverage)),
								'median_cov': sorted(coverage)[len(coverage)/2]}
							record['coverage_table'] = '\n'.join(
									self.summarize(rrna_coverage_schema, name))
						record['run_id'] = self.run_id
						record['img'] = histograms.get(record['name'])
						html.append(self.rrna_summary.safe_substitute(record))
				else:
					html.append(self.header("%s / no targets" % rrna))
			return html

	def rrna_nt(self, summary_file):
		"""Load the nt_summary file, which has fields:
			(query, hit rank, evalue, desc)"""
		nt = dict()
		lines = iter(summary_file)
		# Burn the first line (the header).
		lines.next()
		# Store each line as a query key and (evalue, desc) value.
		for line in lines:
			fields = line.strip().split('\t')
			nt[fields[0]] = (float(fields[2]), fields[3])
		return nt

	def rrna_exemplars(self, fasta_file, nt):
		"""Parse the rRNA exemplars fasta file."""

		records = defaultdict(list)
		piles = dict()

		for record in SeqIO.parse(fasta_file, 'fasta'):
			key = record.name.partition('|')[0]
			name = record.name.partition('|')[2]
			piles[name] = list()
			evalue, desc = nt.get(record.name, (1.0, 'Unknown'))
			desc_fields = desc.split('|')
			if len(desc_fields) > 1:
				url = "http://www.ncbi.nlm.nih.gov/nuccore/" + desc_fields[-2]
			else:
				url = ""
			records[key].append({
				'name': name,
				'evalue': evalue,
				'desc': desc,
				'length': str(len(record)),
				'seq': '>{}|{}|Run{}|{}\n  '.format(key, name, self.run_id, self.id) + \
								'\n  '.join(textwrap.wrap(str(record.seq), 80)),
				'url': url
				})

		return (records, piles)

	def rrna_pileup(self, pileup_file, piles):
		"""Extract the coverage (fourth column) from the pileup file."""
		for line in pileup_file:
			# We only need the first and fourth column.
			fields = line.split('\t', 4)
			for name in piles:
				if fields[0].partition('|')[2] == name:
					piles[name].append(int(fields[3]))

	def rrna_histograms(self, piles):
		"""Create a histogram of the coverage for each contig."""
		histograms = dict()
		for contig, coverage in piles.iteritems():
			if coverage:
				imgname = "%d.%s.hist.png" % (self.run_id, contig)
				ymax = max(coverage)
				props = {
					'figsize': (9,3),
					'title': "Read Depth Across Contig",
					'xlabel': contig + 'Base',
					'ylabel': 'Coverage',
					'yticks': [0, ymax],
					'ylim': [0, ymax]}
				#histograms[contig] = self.histogram(
				#	imgname, np.array(coverage), props=props)
				histograms[contig] = self.lineplot(
					imgname, coverage, props=props)
		return histograms

