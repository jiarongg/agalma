#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Cleans transcripts to remove any rRNA or vector sequences, then selects a
single exemplar transcript for each gene.  Vector sequences could include
untrimmed adapters or plasmids (we sometimes find sequences in our data for the
protein expression vectors used to manufacture the sample preparation enzymes).
Raw reads are mapped back to the exemplars to estimate coverage and assign RPKM
values. Finally, transcripts are annotated with blastx hits against SwissProt.
"""

import numpy as np
import os
import shutil

from Bio import SeqIO
from collections import defaultdict, OrderedDict

from biolite import config
from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import workflows
from biolite import wrappers

from biolite.pipeline import IlluminaPipeline

pipe = IlluminaPipeline('postassemble', __doc__)

# Command-line arguments.

pipe.add_argument('assemblies', short='a', nargs='*',
	default=[], help="""
	A list of contig files with names like 'assembly_<#reads>_<method>.fa'.
	These can be generic assemblies: subset assemblies from agalma or external
	assemblies. The original read data need not be provided.""")

pipe.add_argument('assemblies_full', nargs='*',
	default=[], help="""
	A list of contig files with names like 'assembly_<#reads>_<method>.fa'.
	The original read data must be available, and provided with the --fastq
	parameter or from a previous pipeline run.""")

pipe.add_argument('min_length', short='l', type=int, metavar='L',
	default=300, help="""
	Only keep transcripts longer than L bases.""")

# Pipeline stages.

@pipe.stage
def setup_assemblies(id, previous, data, assemblies, assemblies_full):
	"""Parse assembly paths into a name/path dictionary"""

	if not (assemblies or assemblies_full):
		if previous:
			# Lookup assembly lists from a previous pipeline run.
			values = diagnostics.lookup_prev_run(id, previous)
			try:
				assemblies = diagnostics.str2list(values['assemblies'])
				assemblies_full = diagnostics.str2list(values['assemblies_full'])
			except KeyError:
				utils.die("No assembly lists in diagnostics for previous run.")
		else:
			# Catalog entry is probably for an external assembly, so use the
			# paths in 'data' as the assembly list
			assemblies = data[-1]
	
	assembly_paths = assemblies_full + assemblies
	assemblies = OrderedDict()

	for path in assembly_paths:
		assemblies[utils.basename(path)] = [path]

	if not assemblies:
		utils.die("No assemblies in list.")
	
	assemblies_full = map(utils.basename, assemblies_full)

	ingest('assemblies', 'assemblies_full')

@pipe.stage
def clean_rrna(assemblies, outdir):
	"""Remove rRNA from assembled transcripts"""
	for assembly, assembly_paths in assemblies.iteritems():
		diagnostics.prefix.append(assembly)
		rrna_path = os.path.join(outdir, assembly + '.rrna.fa')
		workflows.clean_rrna(
			assembly_paths[-1],
			assembly + '.norrna.fa',
			rrna_path)
		assembly_paths.append(assembly + '.norrna.fa')
		diagnostics.log_path(rrna_path)
		diagnostics.prefix.pop()

@pipe.stage
def clean_univec(assemblies, outdir):
	"""Remove vector contaminants from assembled transcripts"""
	for assembly, assembly_paths in assemblies.iteritems():
		diagnostics.prefix.append(assembly)
		vector_path = os.path.join(outdir, assembly + '.vectors.fa')
		clean_path = os.path.join(outdir, assembly + '.clean.fa')
		workflows.clean_univec(assembly_paths[-1], clean_path, vector_path)
		diagnostics.log_path(clean_path)
		diagnostics.log_path(vector_path)
		assembly_paths.append(clean_path)
		diagnostics.prefix.pop()

@pipe.stage
def dustmasker(assemblies, outdir):
	"""Remove low complexity regions with DustMasker"""
	for assembly, assembly_paths in assemblies.iteritems():
		diagnostics.prefix.append(assembly)
		dust_path = os.path.join(outdir, assembly + '.dust.fa')
		workflows.dustmasker(
			assembly_paths[-1],
			assembly + '.nodust.fa',
			dust_path)
		assembly_paths.append(assembly + '.nodust.fa')
		diagnostics.log_path(dust_path)
		diagnostics.prefix.pop()

@pipe.stage
def exemplars(assemblies,  min_length, outdir):
	"""Extract exemplar (highest confidence) transcripts from each assembly"""
	for assembly, assembly_paths in assemblies.iteritems():
		diagnostics.prefix.append(assembly)
		exemplars_path = os.path.join(outdir, assembly + '.exemplars.fa')
		workflows.extract_oases_exemplars(
			assembly_paths[-1],
			exemplars_path,
			min_length)
		assembly_paths.append(exemplars_path)
		diagnostics.prefix.pop()

@pipe.stage
def contig_stats(assemblies, outdir):
	"""Calculate contig statistics for assemblies"""
	for assembly, assembly_paths in assemblies.iteritems():
		diagnostics.prefix.append(assembly)
		workflows.contig_stats(
			assembly_paths[-1],
			os.path.splitext(assembly_paths[-1])[0] + '.hist')
		diagnostics.prefix.pop()

@pipe.stage
def coverage(data, assemblies, assemblies_full, outdir):
	"""Build a table of the number of reads covering each transcript"""
	for assembly in assemblies_full:
		diagnostics.prefix.append(assembly)
		coverage_table = os.path.join(outdir, assembly + '.coverage.txt')
		wrappers.Bowtie2Build(
			assemblies[assembly][-1],
			'bowtietranscripts')
		wrappers.Bowtie2(
			data[-1],
			'bowtietranscripts',
			os.path.join(outdir, assembly + '.coverage.sam'),
			sensitive=False, all=False)
		wrappers.Coverage(
			os.path.join(outdir, assembly + '.coverage.sam'),
			coverage_table)
		diagnostics.log_path(coverage_table)
		diagnostics.prefix.pop()

@pipe.stage
def nr_annotate(assemblies, assemblies_full, outdir):
	"""Annotate transcripts with top blast hit"""

	blast_reports = []

	for assembly, assembly_paths in assemblies.iteritems():

		diagnostics.prefix.append(assembly)
		is_full = assembly in assemblies_full

		# Calculated RPKM for full assemblies using the coverage
		if is_full:
			rpkms = workflows.calculate_rpkms(
							os.path.join(outdir, assembly + '.coverage.txt'))
		else:
			rpkms = {}

		# Blastx against SwissProt
		blast_report = os.path.join(outdir, assembly + '.nr.blastx.xml')
		blast_reports.append(blast_report)

		workflows.multiblast(
			'blastx',
			assembly_paths[-1],
			config.get_resource('swissprot_blastdb'),
			blast_report,
			evalue=1e-6,
			cores=1)

		diagnostics.log_path(blast_report, log_prefix='blast')

		nr_hits = workflows.blast_top_hits(blast_report)
		utils.info("%s: %d matches against SwissProt" % (assembly, len(nr_hits)))

		# Annotate hits
		annotated_path = os.path.join(outdir, assembly + '.annotated.fa')
		nr_path = os.path.join(outdir, assembly + '.nr.fa')
		workflows.blast_annotate_seqs(
			nr_hits,
			assembly_paths[-1],
			nr_path,
			annotated_path,
			all_out=True, rpkms=rpkms)

		diagnostics.log_path(annotated_path, log_prefix='annotated')
		assembly_paths.append(annotated_path)

		# Generate contig stats for only the SwissProt hits
		workflows.contig_stats(nr_path, os.path.splitext(nr_path)[0] + '.hist')

		# Generate another coverage table that only include SwissProt hits
		if is_full:
			workflows.filter_coverage_table(
				os.path.join(outdir, assembly + '.coverage.txt'),
				nr_hits,
				os.path.join(outdir, assembly + '.nr.coverage.txt'))

		diagnostics.prefix.pop()

	assembly_paths = assemblies.values()
	assemblies = assemblies.keys()
	finish('assemblies', 'assembly_paths', 'assemblies_full', 'blast_reports')


if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
Field = report.Field
coverage_schema = (
	Field('pairs', 'Read pairs', int, '{:,d}'),
	Field('mapped', 'Pairs mapped', float, '{:.1f}%'),
	Field('discordant', 'Pairs discordant', float, '{:.1f}%'),
	Field('reads_mapped', 'Unpaired reads mapped', float, '{:.1f}%'))

class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('assemblies', diagnostics.EXIT, 'assemblies')
		self.str2list('assemblies')
		self.lookup('assemblies_full', diagnostics.EXIT, 'assemblies_full')
		self.str2list('assemblies_full')
		if 'assemblies_full' in self.data:
			self.data.best_coverage = dict([(x[0],0) for x in coverage_schema])
			self.data.best_coverage['discordant'] = 100
			self.data.coverage_stats = list()
			self.load_coverage_stats()
		# Generators
		self.generator(self.load_assemblies)
		self.generator(self.assemblies)

	### Load functions ###

	def load_hist(self, assembly, values):
		if not values:
			return
		try:
			return np.loadtxt(open(values['path']))
		except IOError:
			utils.info("could not open histogram for assembly '%s', run '%d'" \
													% (assembly, self.run_id))
		except KeyError:
			utils.info("missing histogram entry for assembly '%s', run '%d'" \
													% (assembly, self.run_id))

	def load_coverage(self, assembly, values, coverages):
		"""
		Load coverage table into an array. Fields are:
		0      | 1      | 2        |
		contig | length | coverage |
		"""
		if not values:
			return
		try:
			table = np.loadtxt(open(values['path']), usecols=(1,2))
		except IOError:
			utils.info("could not find histogram for assembly '%s', run '%d'" \
													% (assembly, self.run_id))
			return

		# Normalize coverage by number of bases in the contig.
		norm = table[:,1] / table[:,0]

		# Sort by coverage, descending.
		norm = np.sort(norm)[::-1]

		# Compute cumulative sum of coverage.
		norm = norm.cumsum()

		# Scale to percentage.
		#scale = 100.0 / norm[-1]
		#norm *= scale

		coverages.append(norm)

		# Calculate RPKM as:
		# (1e9 * coverage) / (len * coverage_sum)
		coverage_norm = 1.0 / table[:,1].sum()
		table[:,1] *= 1e9 * coverage_norm
		table[:,1] /= table[:,0]

		return table

	def load_coverage_stats(self):
		for assembly in self.data.assemblies_full:
			best = self.data.best_coverage
			stats = self.data.coverage_stats
			stats.append(dict())
			values = diagnostics.lookup(self.run_id,
								"postassemble.coverage.%s.bowtie2" % assembly)
			if 'npairs' in values:
				stats[-1]['pairs'] = values['npairs']
				best['pairs'] = max(best['pairs'], stats[-1]['pairs'])
				norm = 100.0 / float(values['npairs'])
				if 'nconcord1' in values and 'nconcord2' in values:
					stats[-1]['mapped'] = norm * \
							(int(values['nconcord1']) + int(values['nconcord2']))
					best['mapped'] = max(best['mapped'], stats[-1]['mapped'])
				if 'ndiscord1' in values:
					stats[-1]['discordant'] = norm*int(values['ndiscord1'])
					best['discordant'] = min(best['discordant'], stats[-1]['discordant'])
				if 'nunpaired0' in values:
					stats[-1]['reads_mapped'] = \
										100.0 - 0.5*norm*int(values['nunpaired0'])
					best['reads_mapped'] = max(best['reads_mapped'], stats[-1]['reads_mapped'])

	def split_assembly(self, assembly):
		tokens = assembly.split('_')
		if tokens[0] == 'assembly' and len(tokens) > 2:
			return int(tokens[1]), ' '.join(tokens[2:])
		else:
			return 0, ' '.join(tokens)

	def load_assemblies(self):
		if 'assemblies' in self.data:
			self.data.counts = defaultdict(dict)
			self.data.means = defaultdict(dict)
			self.data.n50s = defaultdict(dict)
			self.data.nr_counts = defaultdict(dict)
			self.data.nr_means = defaultdict(dict)
			self.data.nr_n50s = defaultdict(dict)
			self.data.subsets = set()
			for assembly in self.data.assemblies:
				subset, method = self.split_assembly(assembly)
				self.data.subsets.add(subset)
				values = diagnostics.lookup(self.run_id,
							"postassemble.contig_stats.%s.contig_stats" % assembly)
				if values:
					self.data.counts[method][subset] = int(values['count'])
					self.data.means[method][subset] = float(values['mean'])
					self.data.n50s[method][subset] = int(values['n50'])
				values = diagnostics.lookup(self.run_id,
							"postassemble.nr_annotate.%s.contig_stats" % assembly)
				if values:
					self.data.nr_counts[method][subset] = int(values['count'])
					self.data.nr_means[method][subset] = float(values['mean'])
					self.data.nr_n50s[method][subset] = int(values['n50'])
			self.data.subsets = sorted(self.data.subsets)

		if 'assemblies_full' in self.data:
			self.data.clean_hists = list()
			self.data.swissprot_hists = list()
			self.data.coverages = list()
			self.data.nr_coverages = list()
			self.data.rpkms = list()
			self.data.nr_rpkms = list()
			for assembly in self.data.assemblies_full:
				values = diagnostics.lookup(self.run_id,
							"postassemble.contig_stats.%s.contig_stats" % assembly)
				self.data.clean_hists.append(self.load_hist(assembly, values))
				values = diagnostics.lookup(self.run_id,
							"postassemble.nr_annotate.%s.contig_stats" % assembly)
				self.data.swissprot_hists.append(self.load_hist(assembly, values))
				values = diagnostics.lookup(self.run_id,
										"postassemble.coverage.%s" % assembly)
				self.data.rpkms.append(self.load_coverage(assembly, values, self.data.coverages))
				values = diagnostics.lookup(self.run_id,
					"postassemble.nr_annotate.%s.filter_coverage_table" % assembly)
				self.data.nr_rpkms.append(self.load_coverage(assembly, values, self.data.nr_coverages))

	### Plots ###

	def contig_histogram(self, assembly, clean_hist, swissprot_hist):
		html = [
			"<p>Number of exemplar transcripts in full assembly with blastx hits: {:,d}</p>".format(int(swissprot_hist[:,1].sum()))]
		imgname = "%d.%s.png" % (self.run_id, assembly)
		props = {
			'title': "Exemplar Transcripts - Histogram of Lengths (Full Assembly)",
			'xlabel': "Exemplar Transcript Length (bp)",
			'ylabel': "Frequency"}
			#'yscale': 'log'}
		html.append(
			self.histogram_overlay(imgname,
				[clean_hist, swissprot_hist],
				labels=["Exemplar transcripts", "Exemplar transcripts with blastx hit"],
				props=props))
		return html

	def contig_plot(self, name, title, ylabel):
		if self.check(name):
			data = self.data[name]
			data_bx = self.data['nr_' + name]
			subsets = self.data.subsets
			methods = utils.sorted_alphanum(data.iterkeys())
			y = dict()
			for method in methods:
				y[method] = map(data[method].get, subsets)
			y_bx = dict()
			for method in methods:
				y_bx[method] = map(data_bx[method].get, subsets)
			ymax = max(
					max(map(max, y.itervalues())),
					max(map(max, y_bx.itervalues())))
			# Draw %-% plot.
			imgname = "%d.assemble.%s.png" % (self.run_id, name)
			props = {
				'title': title,
				'xlabel': "Subset size (Number of Read Pairs)",
				'ylabel': ylabel,
				'xticks': [0] + subsets,
				'xlim': [0, 1.1*max(subsets)],
				'ylim': [0, 1.1*ymax],
				'marker': 's',
				'mfc': 'None'}
			plots = [(subsets, y[method], method) for method in methods]
			plots += [(subsets, y_bx[method], method + ' (with blastx hits)') for method in methods]
			return [self.multilineplot(imgname, plots, props)]

	def coverage_plot(self, assembly, cov, nr_cov):
		imgname = "%d.%s.coverage.png" % (self.run_id, assembly)
		props = {
			'title': "Read Coverage Across Exemplar Transcripts",
			'xlabel': "Exemplar Transcripts, ordered from highest to lowest representation",
			'ylabel': "Cumulative mapped reads (RPKM)"}
		plots = (
			(np.arange(cov.size), cov, "all exemplar transcripts"),
			(np.arange(nr_cov.size), nr_cov, "exemplar transcripts with blastx hits"))
		return [self.multilineplot(imgname, plots, props)]

	### Table ###

	def assembly_files(self, assembly):
		links = []
		files = (
			('annotated transcripts', "postassemble.nr_annotate.%s.annotated"),
			('rrna', "postassemble.clean_rrna.%s"),
			('vectors', "postassemble.clean_univec.%s"))
		for label, entity in files:
			src = diagnostics.lookup(self.run_id, entity % assembly).get('path')
			if src and os.path.exists(src):
				dst = '%d.%s' % (self.run_id, os.path.basename(src))
				shutil.copy(src, os.path.join(self.outdir, dst))
				links.append("[<a href=\"%s\">%s</a>]" % (dst, label))
		if links:
			return ', '.join(links)
		else:
			return '-'

	def assembly_table(self):
		html = [
			self.header("Assemblies"),
			'<table class="table table-striped table-condensed table-mini">',
			"<tr>",
			"<th>Method</th><th>Read pairs</th>",
			"<th>Genes</th><th>Mean Length (bp)</th><th>N50 Length (bp)</th>",
			"<th>Links to assembly files</th>",
			"</tr>"]
		for assembly in self.data.assemblies:
			subset, method = self.split_assembly(assembly)
			html.append("<tr>")
			cell = "<td>{}</td>"
			rcell = "<td class=\"right\">{}</td>"
			html.append(cell.format(method))
			html.append(rcell.format('{:,}'.format(int(subset))))
			for stat in ('counts', 'means', 'n50s'):
				try:
					html.append(rcell.format(self.data[stat][method][subset]))
				except KeyError:
					html.append(rcell.format('-'))
			if assembly in self.data.assemblies_full:
				html.append(cell.format(self.assembly_files(assembly)))
			else:
				html.append(cell.format('-'))
			html.append("</tr>")
		html.append("</table>")
		return html

	### Assembly overview ###
	def assemblies(self):
		html = []
		# Table
		if self.check('assemblies', 'assemblies_full', 'subsets'):
			html = self.assembly_table()
			for i, assembly in enumerate(self.data.assemblies_full):
				subset, method = self.split_assembly(assembly)
				html.append(self.header(method, level=1))
				html += self.summarize(coverage_schema, 'coverage_stats', i)
				# Contig histogram
				if self.check('clean_hists', 'swissprot_hists'):
					html += self.contig_histogram(
								assembly,
								self.data.clean_hists[i],
								self.data.swissprot_hists[i])
				# Coverage plot
				if self.check('coverages', 'nr_coverages'):
					html += self.coverage_plot(
								assembly,
								self.data.coverages[i],
								self.data.nr_coverages[i])
			# Contig plots
			# Only run if we have subsets to compare
			if len(self.data.subsets) > 1:
				html.append(self.header("Subset Comparison", level=1))
				html += self.contig_plot(
							'counts',
							"Exemplar Transcripts - Genes",
							"Number of Exemplar Transcripts")
				html += self.contig_plot(
							'means',
							"Exemplar Transcripts - Mean Length",
							"Length (bp)")
				html += self.contig_plot(
							'n50s',
							"Exemplar Transcripts - N50 Length",
							"Length (bp)")
		return html

