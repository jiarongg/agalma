#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Filters raw paired-end Illumina data to remove very low quality read pairs, 
read pairs with adapter sequences, and read pairs with highly skewed base 
composition. It then randomizes the order of reads in the files (applying 
the same order of randomization to each file in the pair) to make it simple 
to get random subsets of read pairs in later analyses. Finally, fastqc is
run to profile the quality of the reads.
"""

import os
import glob
import shutil

from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import wrappers

from biolite.pipeline import IlluminaPipeline

pipe = IlluminaPipeline('sanitize', __doc__)

# Command-line arguments.

pipe.add_argument('quality', short='q', type=int, metavar='MIN',
	default=28, help="""
	Filter out reads that have a mean quality < MIN.""")

# Pipeline stages.

@pipe.stage
def randomize(data):
	"""Randomize the order of the reads"""

	order = 'random.order.txt'
	outputs = []

	for i, input in enumerate(data[-1]):
		outputs.append('random.%d.fastq' % (i+1))
		wrappers.Randomize(input, outputs[-1], i == 0, order)

	data.append(outputs)
	ingest('data')

@pipe.stage
def fastqc(id, data, outdir):
	"""Prepare fastqc report for the reads"""

	for i, input in enumerate(data[-1]):
		# Use the first 100k reads for faster runtime.
		fastqc = "%s.random100k.%d.fastq" % (id, i+1)
		# (4 lines per record * 100k records = 400k lines)
		utils.head_to_file(input, fastqc, n=400000)
		wrappers.FastQC(fastqc, outdir)

@pipe.stage
def sanitize(id, data, outdir, gzip, quality):
	"""Filter out low-quality and adapter-contaminated reads"""

	outputs = []

	for i, _ in enumerate(data[-1]):
		outputs.append(
			os.path.join(outdir, "%s.%d.sanitized.fastq" % (id, i+1)))

	# FIXME: need a mechanism for storing an additional unpaired file in data.
	#outputs.append(os.path.join(outdir, id + '.sanitized.unpaired.fastq'))

	if gzip:
		outputs = [o + '.gz' for o in outputs]

	wrappers.FilterIllumina(
		data[-1],
		outputs,
		#outputs[:-1],
		#unpaired_output=outputs[-1],
		quality=quality, sep='/')

	data.append(outputs)
	finish('data')

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
Field = report.Field
filter_schema = (
	Field('pairs', "Read pairs examined", int, '{:,d}'),
	Field('pairs_kept', "Read pairs kept", int, '{:,d}'),
	Field('percent_kept', "Percent kept", float, '{:.1f}%'),
	Field('quality', "Illumina quality threshold", int, '{:,d}'),
	Field('reject_adapters', "Adapter fails", int, '{:,d}'),
	Field('reject_quality', "Quality fails", int, '{:,d}'),
	Field('reject_content', "Base composition fails", int, '{:,d}'))

class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('outdir', diagnostics.INIT, 'outdir')
		self.lookup('filter', 'sanitize.sanitize.filter_illumina')
		self.percent('filter', 'percent_kept', 'pairs_kept', 'pairs')
		# Generators
		self.generator(self.filter_table)
		self.generator(self.fastqc_table)

	def filter_table(self):
		"""
		
		"""
		if 'filter' in self.data:
			# Pull the quality treshold out of the command arguments.
			self.data.filter['quality'] = self.extract_arg(
									'sanitize.sanitize.filter_illumina', '-q')
			html = [self.header("Illumina Filtering")]
			html += self.summarize(filter_schema, 'filter')
			return html

	def fastqc_table(self):
		"""
		FastQC_ is a tool from Babraham Bioinformatics that generates detailed
		quality diagnostics of NGS sequence data.

		.. _FastQC: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
		"""
		if 'outdir' in self.data:
			fastqc_dirs = glob.glob(os.path.join(self.data.outdir, '*_fastqc'))
			if fastqc_dirs:
				html = [self.header("FastQC reports")]
				html.append("<table cellpadding=\"5\"><tr>")
				html += self.copy_fastqc(fastqc_dirs)
				html.append("</tr></table>")
				return html

	def copy_fastqc(self, fastqc_dirs):
		"""
		Make local copies of fastqc reports.
		"""
		html = list()
		for i in xrange(len(fastqc_dirs)):
			path = fastqc_dirs[i]
			if os.path.isdir(path):
				name = os.path.basename(path)
				newname = "%d.fastqc.%d" % (self.run_id, i+1)
				dest = os.path.join(self.outdir, newname)
				if not os.path.isdir(dest):
					shutil.copytree(path, dest)
				html += [
					"<td>",
					self.header("<a href=\"{0}/fastqc_report.html\">{0}</a>", level=1).format(newname),
					"<pre>"]
				for line in open(os.path.join(path, 'summary.txt')):
					html.append(self.tidy_fastqc_item(line, name))
				html.append("</pre></td>")
		return html

	def tidy_fastqc_item(self, item, fastqc_name):
		# Add colors for pass/fail.
		item = item.replace('PASS', "<span style=\"color: green;\">PASS</span>")
		item = item.replace('FAIL', "<span style=\"color: red;\">FAIL</span>")
		# Tabs to spaces.
		item = item.replace('\t',' ')
		# Remove long name.
		fastqc_name = fastqc_name[:fastqc_name.rfind('_fastqc')]
		item = item[:item.find(fastqc_name)]
		return item

