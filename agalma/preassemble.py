#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Prepares raw paired-end Illumina data for assembly
"""

from biolite import diagnostics

from biolite.pipeline import IlluminaPipeline

# This pipeline is a meta-pipeline that connects the following pipelines:

import sanitize
import insert_size
import remove_rrna

pipe = IlluminaPipeline('preassemble', __doc__)

### sanitize ###

pipe.import_module(sanitize, start=1)

### insert_size ###

pipe.import_module(insert_size, ['subset_size', 'stats_subset_size', 'stats_quality'], start=1)

### remove_rrna ###

pipe.import_module(remove_rrna, ['keep', 'fasta_target'], start=1)

if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

