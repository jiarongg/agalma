#!/usr/bin/env python
#
# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2013 Brown University. All rights reserved.
# 
# This file is part of Agalma.
# 
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Identifies homologous sequences across datasets. Takes assembly or group of
assemblies and prepares a set of comprehensive comparisons between them. First
an all by all BLAST is run with a stringent threshold, and the hits which match
above a given score are used as edges between two transcripts which then form a
graph. This graph is the basis of a series of comparative scores.
"""

import os
import numpy
import networkx

from collections import namedtuple
from glob import glob

import database
import biolite.database

from biolite import diagnostics
from biolite import report
from biolite import utils
from biolite import wrappers
from biolite import workflows

from biolite.pipeline import Pipeline

pipe = Pipeline('homologize', __doc__)

# Command-line arguments.
pipe.add_argument('load_ids', nargs='+', help="""
	Specifies which datasets to analyze; A space delimited list of run_id's
	corresponding to the load runs that populated the database""")
pipe.add_argument('seq_type', default='nucleotide', help="""
	Choose whether comparisons are based on 'nucleotide', 'protein', or
	'masked_protein' sequnces""")
pipe.add_argument('min_bitscore', short='C', type=float, default=200, help="""
	Filter out BLAST hit edges with a bitscore below min_bitscore.""")
pipe.add_argument('min_overlap', short='P', type=float, default=0.5, help="""
	Filter out BLAST hit edges with a max HSP length less than this fraction
	of target length.""")

SpeciesData = namedtuple('SpeciesData', "name ncbi_id itis_id catalog_id")

# Pipeline stages.

@pipe.stage
def lookup_species(load_ids):
	"""Lookup the species data for each run"""

	species = {}

	# Given a run_id, returns a named tuple with the following elements:
	#
	# name		The species names 
	# ncbi_id		The NCBI taxon id
	# itis_id		The ITIS taxon id
	# catalog_id	The agalma catalog id

	for load_id in load_ids:
		row = biolite.database.execute("""
			SELECT catalog.species, catalog.ncbi_id, catalog.itis_id, catalog.id
			FROM catalog, runs
			WHERE runs.run_id=? AND catalog.id=runs.id;""",
			(load_id,)).fetchone()
		if not row:
			utils.die("Couldn't find species data for run ID %s" % load_id)
		if row[0] is None:
			utils.die("Species name is empty for catalog ID '%s'" % row[3])
		species[load_id] = SpeciesData(*row)
		diagnostics.log(str(load_id), row)

	diagnostics.log("species", species)
	ingest('species')


@pipe.stage
def write_fasta(id, _run_id, load_ids, species, seq_type, outdir):
	"""Write the fasta file; also, make a list of nodes in the output directory corresponding sequences."""

	blast_dir = utils.safe_mkdir('allvall_blast_%s_%s' % (id, _run_id))
	fasta = os.path.join(blast_dir, 'all.fa')

	# The nodes file contains a header, which describes the attributes, as well
	# as a line for every node (transcript) in the analysis.
	nodes = os.path.join(outdir, 'nodes.txt')

	nloads = 0
	nseqs = 0
	nbases = 0
	with open(nodes, 'w') as f1, open(fasta, 'w') as f2:
		print >>f1, "label\tid\tassembly\tassembly_number"
		for load_id in load_ids:
			nloads += 1
			for record in database.load_seqs(load_id, species[load_id], seq_type):
				nseqs += 1
				nbases += len(record.seq)

				# The id of the node (the second column) is a unique identifier
				# and because we already have one in the table, use that here.
				print >>f1, "%s\t%d\t%s\t%d" % (
				            record.header, record.id, load_id, nloads)

				print >>f2, '>%d' % record.id
				print >>f2, record.seq

	if not nseqs:
		utils.die("no sequences were written to the FASTA file")

	diagnostics.log_path(nodes, 'nodes')

	diagnostics.log('nloads', nloads)
	diagnostics.log('nseqs', nseqs)
	diagnostics.log('nbases', nbases)

	ingest('blast_dir', 'fasta', 'nodes')


@pipe.stage
def prepare_blast(blast_dir, fasta, seq_type):
	"""Prepare all-by-all BLAST database and command list"""

	if (seq_type == 'masked_protein') or (seq_type == 'protein'):
		dbtype = 'prot'
		program = 'blastp'
	elif seq_type == 'nucleotide':
		dbtype = 'nucl'
		program = 'tblastx'
	else:
		utils.die("unrecognized sequence type '%s'" % seq_type)

	wrappers.MakeBlastDB(dbtype, fasta, os.path.join(blast_dir, 'all'))
	command = program + " -db all -evalue 1e-20" \
	                  + " -outfmt \"6 qseqid sseqid bitscore qlen length\""
	commands = workflows.blast.split_query(fasta, command, 100000, blast_dir)

	ingest('commands')


@pipe.stage
def run_blast(blast_dir, commands, outdir):
	"""Run all-by-all BLAST"""

	blast_hits = os.path.join(blast_dir, 'hits.tab')
	wrappers.Parallel(commands, '--halt', 1, stdout=blast_hits, cwd=blast_dir)
	ingest('blast_hits')


@pipe.stage
def parse_edges(_run_id, blast_hits, min_overlap, min_bitscore):
	"""Parse BLAST hits into edges weighted by bitscore"""

	edges = networkx.Graph()
	edge_file = 'allvall_edges_%s.abc' % _run_id

	for f in glob(blast_hits):
		for line in open(f):
			hit = line.rstrip().split()
			assert len(hit) == 5
			# Sometimes blast outputs a bad query id if the query is longer
			# than 10Kb
			if hit[0].startswith('Query_'):
				utils.info("discarding bad hit with query id '%s'" % hit[0])
				continue
			# fields:
			# qseqid sseqid bitscore qlen length
			id_from = int(hit[0])
			id_to = int(hit[1])
			bitscore = float(hit[2])
			length = 3.0 * float(hit[3]) / float(hit[4])
			# Filter out self hits, low scoring hits, and short hits
			if id_from != id_to and length > min_overlap and bitscore > min_bitscore:
				# If an edge already exists between the nodes, update its
				# score to be the max
				if edges.has_edge(id_from, id_to):
					e = edges.edge[id_from][id_to]
					e['score'] = max(e['score'], bitscore)
				else:
					edges.add_node(id_from)
					edges.add_node(id_to)
					edges.add_edge(id_from, id_to, score=bitscore)

	with open(edge_file, 'w') as f:
		for id_from, id_to in edges.edges_iter():
			print >>f, "%d\t%d\t%f" % (id_from, id_to, edges.edge[id_from][id_to]['score'])

	utils.info("graph nodes=%d edges=%d" % (edges.number_of_nodes(), edges.number_of_edges()))

	ingest('edge_file')


@pipe.stage
def mcl_cluster(edge_file, outdir):
	"""Run mcl on all-by-all graph to form gene clusters"""
	cluster_file = os.path.join(outdir, 'mcl_gene_clusters.abc')
	wrappers.Mcl(edge_file, cluster_file);
	ingest('cluster_file')


@pipe.stage
def load_mcl_cluster(_run_id, cluster_file):
	"""Load cluster file from mcl into homology database"""

	hist = {}

	database.execute("BEGIN")
	database.execute("DELETE FROM homology WHERE run_id=?;", (_run_id,))
	with open(cluster_file, 'r') as f:
		cluster_id = 0
		for line in f:
			cluster = line.split()
			l = len(cluster)
			hist[l] = hist.get(l, 0) + 1
			for seq_id in cluster:
				database.execute("""
					INSERT INTO homology (run_id, component_id, sequence_id)
					VALUES (?,?,?);""",
					(_run_id, cluster_id, seq_id))
			cluster_id += 1
	database.execute("COMMIT")

	utils.info("histogram of gene cluster sizes:")
	for k in sorted(hist):
		print "%d\t:\t%d" % (k, hist[k])

	diagnostics.log('histogram', hist)


if __name__ == "__main__":
	# Run the pipeline.
	pipe.run()
	# Push the local diagnostics to the global database.
	diagnostics.merge()

# Report. #
class Report(report.BaseReport):
	def init(self):
		self.name = pipe.name
		# Lookups
		self.lookup('load_ids', diagnostics.INIT, 'load_ids')
		self.str2list('load_ids')
		self.lookup('species', 'homologize.lookup_species')
		self.lookup('nseqs', 'homologize.write_fasta', 'nseqs')
		# Generators
		self.generator(self.species_table)
		self.generator(self.component_histogram)

	def species_table(self):
		"""
		Summary of all species processed.
		"""
		html = []
		if self.check('nseqs'):
			html.append("<p>Total sequences: %s</p>" % self.data.nseqs)
		if self.check('load_ids', 'species'):
			headers = ("Load ID", "Catalog ID", "Species", "NCBI ID", "ITIS ID")
			rows = []
			for load_id in self.data.load_ids:
				row = self.data.species.get(str(load_id), '()')
				row = diagnostics.str2list(row)
				if len(row) == 4:
					rows.append((load_id, row[3], row[0], row[1], row[2]))
				else:
					rows.append((load_id, '-', '-', '-', '-'))
			html += self.table(rows, headers)
		return html

	def component_histogram(self):
		"""
		Distribution of the number of nodes in each cluster.
		"""
		hist = {}
		sql = """
			SELECT component_id, COUNT(*)
			FROM homology
			WHERE run_id=?
			GROUP BY component_id;"""
		for id, count in database.execute(sql, (self.run_id,)):
			hist[count] = hist.get(count, 0) + 1
		if hist:
			hist = [(k,hist[k]) for k in sorted(hist.iterkeys())]
			imgname = "%d.component.hist.png" % self.run_id
			props = {
				'title': "Distribution of Cluster Sizes",
				'xlabel': "# Nodes in Cluster",
				'ylabel': "Frequency"}
			return [self.histogram_overlay(imgname, [numpy.array(hist)], props=props)]

