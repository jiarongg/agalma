Agalma Tutorial
===============

This tutorial has two sections. The first describes an assembly run of the
Agalma transcriptome pipeline using a sample read data set. The second
describes a run of the phylogeny pipeline on a set of sample transcriptomes.

Let's start by creating an organized directory hierarchy in your home directory
for storing Agalma data:

    mkdir -p ~/agalma/analyses
    mkdir -p ~/agalma/data
    mkdir -p ~/agalma/scratch

You now have a directory structure like:

    ~
    |-- agalma
        | -- analyses
        | -- data
        | -- scratch

Agalma distinguishes between three types of storage locations:

1. a permanent location for raw data ("data")
2. a permanent location for the final output from analyses ("analyses")
3. a temporary location for the intermediate files from analyses ("scratch")

In a production system, you would want to make sure that "analyses" and "data"
are backed up, but "scratch" doesn't need to be backed up because its files
are typically only used over the course of a single analysis and can be
regenerated.

The 'testdata' command will download all of the sample data sets used in the
tutorials (181 MB compressed):

* A subset of 1 million Illumina HiSeq reads from the species
  *Hippopodius hippopus*
* A smaller subset of 25 thousand Illumina HiSeq reads, also from *Hippopodius*
* 6 transcriptome subsets, from Agalma assemblies of 5 Illumina HiSeq data sets
  and one public/external transcriptome

Let's run this command to download the sample data to the "data" directory:

    cd ~/agalma/data
    agalma testdata

You should now see 10 files in that directory:

    HippopodiusTest1.fastq
    HippopodiusTest2.fastq
    HippopodiusTestSmall1.fastq
    HippopodiusTestSmall2.fastq
    HWI-ST625-54-C026EACXX-6-ATCACG.fa
    HWI-ST625-54-C026EACXX-6-CAGATC.fa
    HWI-ST625-54-C026EACXX-6-CTTGTA.fa
    HWI-ST625-54-C026EACXX-6-GCCAAT.fa
    HWI-ST625-54-C026EACXX-6-TTAGGC.fa
    JGI_NEMVEC.fa


Transcriptome
-------------

Now we'll create a catalog entry for the read data set. You choose between 
either the 1M or 25K read data sets. To do this, you will run one of the 
following two commands.

The 1M reads requires roughly 17 GB of RAM and 2.7 hours on an 8-core Intel
Nehalem system. To catalog them, use:

    agalma catalog insert -p HippopodiusTest1.fastq HippopodiusTest2.fastq -s "Hippopodius hippopus" -d 51331 -n 168745 -q "Illumina HiSeq 2000" -c "Brown Genomics Core"

The 25K reads requires roughly 4 GB of RAM and 5.5 minutes on 2 cores of a
MacBook Pro Retina 15". To catalog them, use:

    agalma catalog insert -p HippopodiusTestSmall1.fastq HippopodiusTestSmall2.fastq -s "Hippopodius hippopus" -d 51331 -n 168745 -q "Illumina HiSeq 2000" -c "Brown Genomics Core"


Note in the above commands that we are specifying both the the 
[ITIS id](http://www.itis.gov), 
which is 51331, and [NCBI id](http://www.ncbi.nlm.nih.gov/taxonomy), which is 
168745, for this species. Specifying both id's is highly recommended.

The catalog command automatically recognizes the Casava 1.8 header in the data
and assigns a catalog ID `HWI-ST625-54-C026EACXX-8-ATCACG`.

Before running a pipeline, you may want to adjust the concurrency and memory
for the node you are running on. System performance may lag if you allocate the 
full available memory to agalma. You should leave at least 2 GB unallocated. If,
for example, you have a 4-core machine with 24 GB of free memory, you could 
allocate all the processors and 20 GB of the memory:

    export BIOLITE_RESOURCES="threads=4,memory=20G"

You can probably run the 25K reads with less memory, for instance using only
2GB with:

    export BIOLITE_RESOURCES="memory=2G"

Now run the full transcriptome pipeline on the test data from the scratch
directory, and specify 'analyses' as the permanent output directory, with:

    cd ~/agalma/scratch
    agalma transcriptome --id HWI-ST625-54-C026EACXX-8-ATCACG --outdir ~/agalma/analyses

When the pipeline finishes you will see it print:

    Report is in 'HWI-ST625-54-C026EACXX-8-ATCACG.Hippopodius_hippopus.report.zip'

You can view the report by opening the HTML file in a web browser on Linux
(GNOME desktop) with:

    gnome-open transcriptome-1/HWI-ST625-54-C026EACXX-8-ATCACG.Hippopodius_hippopus.report/index.html

Or on OS X with:

    open transcriptome-1/HWI-ST625-54-C026EACXX-8-ATCACG.Hippopodius_hippopus.report/index.html

The final products of the transcriptome pipeline are located in:

    ~/agalma/analyses/HWI-ST625-54-C026EACXX-8-ATCACG/1

Note that the '1' may be different if you started the pipeline multiple times
while working through the TUTORIAL, and will correspond to the run id.


Phylogeny
---------

This analysis builds a species tree for 6 taxa using small transcriptome
subsets, and requires roughly 400 MB of RAM and 5 minutes on 2 cores of a
MacBook Pro Retina 15".

Let's define a default value for the output directory so that you don't have to
keep retyping the `--outdir` parameter:

    export BIOLITE_RESOURCES="threads=4,memory=20G,outdir=~/agalma/analyses"

Rather than assemble all of the datasets, we are going to use the fasta 
files from already assembled transcriptomes.

First, we'll catalog our transcriptome fasta files:

    cd ~/agalma/data
    agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-ATCACG" --species "Abylopsis tetragona" --ncbi_id "316209" --paths HWI-ST625-54-C026EACXX-6-ATCACG.fa
    agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-CAGATC" --species "Cordagalma sp." --itis_id "51393" --paths HWI-ST625-54-C026EACXX-6-CAGATC.fa
    agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-CTTGTA" --species "Diphyes dispar" --ncbi_id "316177" --paths HWI-ST625-54-C026EACXX-6-CTTGTA.fa
    agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-GCCAAT" --species "Chuniphyes multidentata" --ncbi_id "316200" --paths HWI-ST625-54-C026EACXX-6-GCCAAT.fa
    agalma catalog insert --id "HWI-ST625-54-C026EACXX-6-TTAGGC" --species "Apolemia sp." --itis_id "51421" --paths HWI-ST625-54-C026EACXX-6-TTAGGC.fa
    agalma catalog insert --id "JGI_NEMVEC" --species "Nematostella vectensis" --ncbi_id "45351" --itis_id "52498" --paths JGI_NEMVEC.fa

Each assembled transcriptome you want to use has to be loaded separately into
the Agalma database using the `load` pipeline. Here is an example of loading
transcriptomes for the 5 sample data sets that were assembled with Agalma. The
`load` pipeline automatically uses the path in the catalog as the location of
the FASTA file containing the contigs.

    cd ~/agalma/scratch
    agalma load --id HWI-ST625-54-C026EACXX-6-ATCACG
    agalma load --id HWI-ST625-54-C026EACXX-6-CAGATC
    agalma load --id HWI-ST625-54-C026EACXX-6-CTTGTA
    agalma load --id HWI-ST625-54-C026EACXX-6-GCCAAT
    agalma load --id HWI-ST625-54-C026EACXX-6-TTAGGC

Since JGI_NEMVEC wasn't assembled with Agalma, it doesn't yet have annotations
for BLASTX hits against SwissProt. To annotate it, use the `postassemble`
pipeline, skipping the exemplar selection stage:

    agalma postassemble --id JGI_NEMVEC --skip exemplars

Since JGI_NEMVEC uses a different convention for the FASTA headers, we need
to load it using the `--generic` flag. To load the annotated transcripts from
the postassemble run, we need to know its run id, which you can find as the last
entry in:

    agalma diagnostics list

If this is the first analysis you have run, the run id would be 6:

    agalma load --id JGI_NEMVEC --generic --previous 6

Now that you have loaded all of the data sets, call the `homologize` pipeline
on the list of run IDs for each of the load commands. If the load commands
above were the first calls you made to agalma, they would have run IDs numbered
1 through 7 (skipping 6 for postassemble), and your homologize command is:

    agalma homologize --load_ids 1 2 3 4 5 7 --seq_type nucleotide --id PhylogenyTest

You may need to adjust the load ids if you have run other analyses, for example
in the transcriptome section above. The option `--id PhylogenyTest` specifies the
analysis id, and will be used to pass results through several 
commands bellow. This id should be unique for each phylogenetic analysis.

The `homologize` pipeline will write a collection of gene clusters back into
the Agalma database.

Now call this sequence of pipelines, inserting the analysis ID each time, to build
gene trees for each of the clusters, and finally to assemble a supermatrix with
a single concatenated sequence of genes for each taxon. Adjust the first run
ID to be that of the homologize run above.

    agalma multalign --id PhylogenyTest
    agalma genetree --id PhylogenyTest
    agalma treeprune --id PhylogenyTest
    agalma multalign --id PhylogenyTest --supermatrix
    agalma genetree --id PhylogenyTest

The supermatrix will be located in:

    ~/agalma/analyses/NoID/PhylogenyTest/supermatrix-nuc/supermatrix.fa
    ~/agalma/analyses/NoID/PhylogenyTest/supermatrix-prot/supermatrix.fa

and the phylogenetic tree in:

    ~/agalma/analyses/NoID/PhylogenyTest/raxml-trees/

