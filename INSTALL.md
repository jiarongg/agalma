Installation
============

Requirements
------------

Agalma is optimized to run on one node with at least 8 cores and a large amount
of RAM. The RAM should be about one and a half times as large as your input
files.

Software dependencies:

* [BioLite 0.3.1](https://bitbucket.org/caseywdunn/biolite)

Please follow the INSTALL instructions for BioLite before these instructions.


Installing from the tar ball
----------------------------

Unpack the tarball (from https://bitbucket.org/caseywdunn/agalma/downloads):

    tar xf agalma-X.X.X.tar.gz
    cd agalma-X.X.X

Run the configure script (use -h to see the available options):

    ./configure

After configuring, install with:

    sudo make install

This installs Agalma to /usr/local by default.  If you would like to install to
a location other than /usr/local (if you don't have permission to write to
/usr/local, for example), see the "Installing to an alternative location"
section.

Proceed to "Installing BLAST databases"


Installing from the git repo
----------------------------

(Skip this unless you are building a development version that you cloned from
Bitbucket.)

First, create a fork of the agalma repository so that you can later send a pull
request so that we can incorporate your improvements. Then clone your forked
repository.

You will need to have the automake, autoconf and libtool packages installed.
To produce the configure scripts, use the command:

    ./autogen.sh

Then you can use the typical build sequence:

    ./configure
    sudo make install

Proceed to "Installing BLAST databases"


Installing BLAST databases
--------------------------

To download and install BLAST databases that have been optimized for use
with Agalma, run the command:

    agalma blastdb

You may need to prefix this command with `sudo` if you installed Agalma to
a location that you don't have access write to (like `/usr/local`).

Follow the instructions in
[TUTORIAL](https://bitbucket.org/caseywdunn/agalma/src/master/TUTORIAL.md)
to test your installation and see how to use Agalma.


Installing to an alternative location
-------------------------------------

The instructions above assume that you are installing Agalma to `/usr/local`,
which requires root access. If you are installing to another location, modify
the installation instructions as follows. These are not full instructions, they
just explain how to modify the instructions above.

In the instructions below, we use `[installation_path]` as a placeholder for the
path that you would like to install to, e.g. `/home/lucy/local`. Note that
`[installation_path]` needs to be an absolute path.

Modify the configure command to changethe install location:

    ./configure --prefix=[installation_path]

Proceed to the instructions below for setting paths, then with make and make
install.


Setting PATH
------------

If you install to a canonical location on your system, like `/usr/local`, the
`agalma` script will already be in your PATH.  Otherwise, you need to add the
new `bin` directory to your PATH so that you can call `agalma`. In `bash` (you
can add add this to `~/.bashrc`):

    export PATH=[installation_path]/bin:$PATH

or in `csh`:

    setenv PATH [installation_path]/bin:$PATH

Note that if you are installing at the same alternate location used by BioLite,
the directory may already be in your path.


Generating a tarball from the git repo
--------------------------------------

You can generate a tarball with the commands:

    ./configure
    make dist-gzip

