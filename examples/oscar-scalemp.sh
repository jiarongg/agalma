#!/bin/bash
#PBS -t 1
#PBS -l nodes=1:ppn=9,walltime=24:00:00
#PBS -j eo
#PBS -q scalemp

module load agalma/0.2.1 vsmp

export BIOLITE_CONFIG=/gpfs/data/cdunn/analyses/biolite.cfg
export BIOLITE_RESOURCES="nt_blastdb=/ramfs/blastdb/nt,swissprot_blastdb=/ramfs/blastdb/swissprot"

# To generate the ID file, use:
#
#   agalma catalog sizes | cut -f 3 | uniq >ids.txt
#
# This file must be in the same directory that you qsub the script from.

IDS=ids.txt
ID=`awk "NR==$PBS_ARRAYID" $IDS`

OUTDIR=/gpfs/data/cdunn/analyses/transcriptomes

#WORKDIR=/ramfs/cdunn/$ID
WORKDIR=$HOME/scratch/$ID
mkdir -p $WORKDIR

cd $WORKDIR

echo $WORKDIR
echo $LD_PRELOAD

set -o errexit

smprun -n 8 agalma sanitize -i $ID -o $OUTDIR
smprun -n 8 agalma insert_size -i $ID -o $OUTDIR -p $ID
smprun -n 8 agalma remove_rrna -i $ID -o $OUTDIR -p $ID
smprun -n 8 agalma assemble -i $ID -o $OUTDIR -p $ID

