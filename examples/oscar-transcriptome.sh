#!/bin/bash
#PBS -l nodes=1:ppn=8,walltime=12:00:00
#PBS -j eo
#PBS -q timeshare

module load agalma/current

ulimit -c unlimited

ID=HWI-ST625:54:C026EACXX:6:TTAGGC

OUTDIR=/gpfs/data/cdunn/analyses/transcriptomes/
mkdir -p $OUTDIR

WORKDIR=$HOME/scratch/$ID
mkdir -p $WORKDIR

cd $WORKDIR
echo $WORKDIR
agalma transcriptome --id $ID --outdir $OUTDIR

